
# RSDDP

Using Randomized Smoothing and Differential Dynamic Programming to solve optimal control problems with non-smooth dynamics.

![](images/random_solo_annotated.png "Randomized smoothing on Solo robot")

# Requirements

This work uses:
- [diffqcqp](https://github.com/quentinll/diffqcqp) (Differentiable QP/QCQP solver),
- [diffsim](https://github.com/lmontaut/diffsim) (Differentiable simulator),
- [Crocoddyl](https://github.com/loco-3d/crocoddyl) (Efficient DDP solver).
