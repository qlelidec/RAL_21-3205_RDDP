#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 23 09:14:45 2021

@author: qlelidec
"""
class FrictionDPEnv(gym.Env):
    
    def __init__(self, friction, dt, T, target = np.array([0.,0.,1.]),w_xterm = 1e-0, w_ureg = 1e-1, w_xcurr= 1e-7, w_xreg = 1e-7):
        super(FrictionDPEnv, self).__init__()
        #defining dynamical system
        robot = erd.load("double_pendulum")
        self.model = robot.model
        self.data = robot.data
        self.geom_model = robot.collision_model
        self.geom_data = robot.collision_data
        self.action_space = gym.spaces.Box(low=-np.inf, high=np.inf,
                                        shape=(self.model.nv,), dtype=np.double)
        self.observation_space = gym.spaces.Box(low=-np.inf, high=np.inf,
                                        shape=(self.model.nq + self.model.nv,), dtype=np.double)
        self.friction = friction
        self.init_state = np.zeros(self.model.nq + self.model.nv)+ .5*np.pi
        self.state = self.init_state
        self.instant = 0
        self.dt = dt
        self.T = T
        #defining cost weights
        self.target = target
        self.w_xterm = w_xterm
        self.w_ureg = w_ureg
        self.w_xcurr = w_xcurr
        self.w_xreg = w_xreg
        #define visualizer for rendering
        name = "pendulum"
        self.viz = MeshcatVisualizer(env.model, env.geom_model, env.geom_model)
        self.viz.initViewer()
        self.viz.loadViewerModel(name)
        self.viz.display(self.state[:self.model.nq])
        return 

    def step(self, action):
        q,v = self.RK4Update_with_friction(action)
        pin.forwardKinematics(self.model, self.data, q, v)
        pin.updateFramePlacements(self.model, self.data)
        self.state = np.concatenate((q,v), axis= 0)
        observation = np.concatenate((q,v), axis=0)
        self.instant += 1
        if self.instant == self.T:
            done = True
        else:
            done = False
        reward = self.compute_reward(self.state, action)
        info = {}
        return self.state, reward, done, info

    def reset(self):
        self.instant = 0 
        self.state = self.init_state
        return self.state

    def render(self):
        self.viz.display(self.state[:self.model.nq])
        return
    
    def compute_reward(self, state, action):
        reward = - self.w_ureg*.5*(np.linalg.norm(action)**2)
        reward += - self.w_xreg*.5*(np.linalg.norm(action)**2)
        if self.instant == self.T:
            reward += - self.w_xterm*.5*(np.linalg.norm(self.data.oMi[1].translation - self.target)**2)
        else:
            reward += - self.w_xcurr*.5*(np.linalg.norm(self.data.oMi[1].translation - self.target)**2)
        return reward
    
    def aba_with_friction(self,model,data,q,v,u,cf):
        a = pin.aba(model,data,q,v,u)
        Minv = np.linalg.inv(pin.crba(model,data,q))
        af = np.where(np.abs(v) < 1e-3, 
                      np.sign(a)*np.clip(np.abs(a)-np.abs(np.dot(Minv,np.sign(a)*cf)),0,None),
                      a-np.dot(Minv,np.sign(v)*cf))
        return af
    
    def RK4Update_with_friction(self,u):
        cf = self.friction*np.ones(u.shape)
        q,v = self.state[:self.model.nq], self.state[-self.model.nv:]
        a1 = self.aba_with_friction(self.model,self.data,q,v,u,cf)
        q1,v1 = pin.integrate(self.model, q,v*self.dt/2), v+a1*self.dt/2
        a2 = self.aba_with_friction(self.model,self.data,q1,v1,u,cf)
        q2,v2 = pin.integrate(self.model, q,v1*self.dt/2), v+a2*self.dt/2
        a3 = self.aba_with_friction(self.model,self.data,q2,v2,u,cf)
        q3,v3 = pin.integrate(self.model, q,v2*self.dt), v+a3*self.dt
        a4 = self.aba_with_friction(self.model,self.data,q3,v3,u,cf)
        return pin.integrate(self.model, q,(self.dt/3)*((1/2)*v+v1+v2+(1/2)*v3)), v+ (self.dt/3)*((1/2)*a1 + a2 + a3 + (1/2)*a4)

    
    def close(self):
        return


from stable_baselines3 import PPO
import matplotlib.pyplot

friction = 0.01
T = 1500
dt = 0.001
env = FrictionDPEnv(friction, dt, T)

from stable_baselines3.common.env_checker import check_env
check_env(env)

from stable_baselines3.common.callbacks import EvalCallback

eval_env = FrictionDPEnv(friction, dt, T)
eval_callback = EvalCallback(eval_env, best_model_save_path='./logs/',
                             log_path='./logs/', eval_freq=T*5,
                             deterministic=True, render=False)

model = PPO("MlpPolicy", env, verbose=0)
model.learn(total_timesteps=T*200, callback= eval_callback)

res_train =  np.load("logs/evaluations.npz")
plt.figure()
plt.plot(res_train["timesteps"], res_train["results"][:,1], label = "reward over episode")
plt.legend()
plt.show()

name = "pendulum"
viz = MeshcatVisualizer(env.model, env.geom_model, env.geom_model)
viz.initViewer()
viz.loadViewerModel(name)
viz.display(q_init)

obs = env.reset()
states =[]
for i in range(T):
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.step(action)
    states += [obs]
    
for t,obs in enumerate(states):
    q = obs[:env.model.nv]
    env.viz.display(q)
    if t==0:
        time.sleep(3.)
    time.sleep(dt)