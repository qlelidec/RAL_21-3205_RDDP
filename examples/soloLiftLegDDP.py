#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  4 14:23:24 2021

@author: qlelidec
"""

import pinocchio as pin
import numpy as np
import torch
from torch.autograd.functional import jacobian
import crocoddyl as croc
import example_robot_data
import matplotlib.pyplot as plt
from diffsim.shapes import Ellipsoid, Plane 
from pinocchio.visualize import MeshcatVisualizer
from diffsim.collision_pairs import CollisionPairPlaneEllipsoid
from diffsim.utils_render import init_viewer_ellipsoids
from diffsim.simulator import Simulator,SimulatorNode
from tqdm import trange, tqdm
from pathlib import Path
import os
from utils.utils import make_video, createSoloModel, SmoothActionModel
import argparse



path_curr = Path().cwd()

EXP_TYPE = "lift_leg"

default_params = {"solo":{"lr":.5,"n_iter":20,"DT":5e-3, "T":20, "nb_samples":4,"noise_intensity":7e-1, "wu_reg": 5., "wx_final": 1e3, "optimizer":"SGD", "adapt_smoothing": "None", "save_path":path_curr/'logs'/'solo_lifting_leg_test'}}
EXP_ID = 0
LEARNING_RATE = default_params["solo"]["lr"]
NUM_ITER = default_params["solo"]["n_iter"]
OPTIMIZER = default_params["solo"]["optimizer"]
DT = default_params["solo"]["DT"]
TIME_HORIZON = default_params["solo"]["T"]
NB_SAMPLES = default_params["solo"]["nb_samples"]
NOISE_INTENSITY = default_params["solo"]["noise_intensity"]
ADAPTIVE_SMOOTHING = default_params["solo"]["adapt_smoothing"]
WU_REGULARIZATION = default_params["solo"]["wu_reg"]
WFINAL_STATE = default_params["solo"]["wx_final"]
SAVE_PATH = default_params["solo"]["save_path"]

parser = argparse.ArgumentParser()
parser.add_argument('-et', '--experiment-type', type=str, default=EXP_TYPE)
parser.add_argument('-eid', '--experiment-id', type=int, default=EXP_ID)
parser.add_argument('-lr', '--learning-rate', type=float, default=LEARNING_RATE) 
parser.add_argument('-ni', '--num-iter', type=int, default=NUM_ITER)
parser.add_argument('-opt', '--optimizer', type=str, default=OPTIMIZER)
parser.add_argument('-nbs', '--nb-samples', type=float, default=NB_SAMPLES) 
parser.add_argument('-noi', '--noise-intensity', type=float, default=NOISE_INTENSITY) 
parser.add_argument('-T', '--time-horizon', type=int, default=TIME_HORIZON) 
parser.add_argument('-dt', '--time-step', type=float, default=DT) 
parser.add_argument('-ada', '--adaptive-smoothing', type=str, default=ADAPTIVE_SMOOTHING) 
parser.add_argument('-wur', '--wu-regularization', type=str, default=WU_REGULARIZATION) 
parser.add_argument('-wfs', '--wfinal-state', type=str, default=WFINAL_STATE) 
parser.add_argument('-svp', '--save-path', type=str, default=SAVE_PATH) 



args = parser.parse_args()

rmodel, rgeomModel, rdata, rgeom_data = createSoloModel()


#theta =  0# np.pi /6
#c,s = np.cos(theta), np.sin(theta)
#rty = np.array([[c,0.,s],
#               [0.,1.,0.],
#               [-s,0.,c]])
#n = np.array([s,0.,c])
#tx, ty =  np.array([c,0.,-s]), np.array([0.,1.,0.])
#gpo = pin.SE3(rty,np.matrix([0.,0.,0.]).T)
q_init = rmodel.qref

v_init = np.zeros(rmodel.nv)
pin.forwardKinematics(rmodel, rdata, q_init)
pin.updateGeometryPlacements(rmodel, rdata, rgeomModel, rgeom_data)

q0 = q_init.copy()
q_target = q_init.copy()
q_target[-1] += .9
dq0 = pin.difference(rmodel, rmodel.qref, q0)
dqrobot0Ref = dq0[:rmodel.nv-6].copy()
v0 = np.zeros(rmodel.nv) 
#v0[rmodel.nv-6:] = np.zeros(6)
x0 = np.zeros(2*rmodel.nv)
x0[:rmodel.nv] = dq0
x0[rmodel.nv:] = v0
dqtarget = pin.difference(rmodel, rmodel.qref, q_target)
vtarget = v0.copy()

coeff_friction = 0.7
coeff_rest = 0.0
dt = args.time_step
T = args.time_horizon

sim_nodes = [SimulatorNode( Simulator(rmodel, rgeomModel, dt, coeff_friction, coeff_rest, steps_contact_cascade = 20, dt_collision=dt, k_baumgarte=.1))]*T
sim_nodes2 = [SimulatorNode( Simulator(rmodel, rgeomModel, dt, coeff_friction, coeff_rest, steps_contact_cascade = 10, dt_collision=dt, k_baumgarte=.1))]*T
N_iter = int(args.num_iter)
ddl=rmodel.nv-6
adaptive_smoothing = True
theta_ad = .001
N_samples, noise_intensity = args.nb_samples, args.noise_intensity
alpha = args.learning_rate
wu = args.wu_regularization
ws = args.wfinal_state

u_init = np.load("utils/solo12_quasistatic_u.npy")

loss_values = []
noise_intensities = []
N_samples_values = []
contact_forces = np.zeros((T,))
positions = np.zeros((N_iter,T,rmodel.nv))
forces_values = np.zeros((N_iter,T,ddl))
gradient_values = np.zeros((N_iter, T, ddl))

state = croc.StateVector(2*rmodel.nv)
jointBaseId = 1
sim = Simulator(rmodel, rgeomModel, dt, coeff_friction, coeff_rest, dt_collision=dt, steps_contact_cascade=10)
actionModel1 = SmoothActionModel(state, sim, jointBaseId, dqtarget, 0, wu, dqrobot0Ref, noise_intensity, N_samples, terminal=False)
actionModelTerminal = SmoothActionModel(state, sim, jointBaseId, dqtarget, ws, wu, dqrobot0Ref, noise_intensity, N_samples, terminal=True)
problem = croc.ShootingProblem(x0, [actionModel1]*T,
                               actionModelTerminal)

ddp = croc.SolverFDDP(problem)
log = croc.CallbackLogger()
ddp.setCallbacks([log, croc.CallbackVerbose()])

init_us = [u_init] * T
init_xs = problem.rollout(init_us)
done = ddp.solve(init_xs, init_us, 20)
print(done)

os.makedirs(path_curr/'logs'/'solo_lifting_leg_rsddp', exist_ok=True)
u = torch.tensor(ddp.us.tolist())
torch.save(u,path_curr/'logs'/'solo_lifting_leg_rsddp'/'u.pt')

traj = np.zeros((T+1,rmodel.nq))
contact_forces = []
xnext = torch.cat((torch.tensor(dq0),torch.tensor(v0)), dim=0).detach().clone()

pattes_positions = []
acc_com = []
masstot = 0
for bodymass in rmodel.inertias.tolist():
    masstot += bodymass.mass
traj[0,:] = rmodel.qref
for t in range(T):
    t_tau = torch.zeros(rmodel.nv)
    t_tau[6:] = u[t]
    xnext = sim_nodes[t].makeStep(xnext, t_tau, calcPinDiff=False)
    pin.updateGeometryPlacements(rmodel, sim_nodes[t].rdata, rgeomModel, rgeom_data)
    traj[t+1,:] = pin.integrate(rmodel, rmodel.qref,xnext[:rmodel.nv].detach().numpy())
    contact_forces += [sim_nodes[t].rdata.contact_forces]
    acc_com += [sum(sim_nodes[t].rdata.contact_forces[:,2])/(masstot*dt) - 9.81]
    pattes_positions += [np.array([rgeom_data.oMg[-1].translation[2], rgeom_data.oMg[-2].translation[2], rgeom_data.oMg[-3].translation[2], rgeom_data.oMg[-4].translation[2]])]
    
plt.figure()
plt.plot([i for i in range(traj.shape[0])], traj[:,2], color = '#348ABD')
plt.xlabel("Time step")
plt.ylabel("Altitude")

plt.figure()
for k in range(contact_forces[0].shape[0]):
    plt.plot([i for i in range(len(contact_forces))], [contact_force[k,2] for contact_force in contact_forces])
plt.xlabel("Time step")
plt.ylabel("contact forces")
plt.savefig(path_curr/'logs'/'solo_lifting_leg_rsddp'/'contact_forces_values.pdf')

plt.figure()
for k in range(pattes_positions[0].shape[0]):
    plt.plot([i for i in range(len(pattes_positions))], [patte_position[k] for patte_position in pattes_positions])
plt.xlabel("Time step")
plt.ylabel("Positions of feet")
plt.savefig(path_curr/'logs'/'solo_lifting_leg_rsddp'/'feet_values.pdf')

plt.figure()
plt.plot([i for i in range(len(acc_com))], acc_com, color = '#348ABD')
plt.xlabel("Time step")
plt.ylabel("Acc CoM (m/s^2)")
plt.savefig(path_curr/'logs'/'solo_lifting_leg_rsddp'/'CoMacc_values.pdf')

name = "solo_lift_leg"
viz_lift = init_viewer_ellipsoids(rmodel, rgeomModel, open=False)
fps = 5.
os.makedirs(args.save_path, exist_ok=True)
make_video(viz_lift, np.concatenate((np.expand_dims(q_init,axis=0),traj), axis = 0), args.save_path/'ARS-GD-LS.mp4', fps)
