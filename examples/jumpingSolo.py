
import pinocchio as pin
import numpy as np
import torch
import hppfcl
import copy
import crocoddyl as croc
import time
import example_robot_data
import matplotlib.pyplot as plt
from diffsim.shapes import Ellipsoid, Plane 
from pinocchio.visualize import MeshcatVisualizer
from diffsim.collision_pairs import CollisionPairPlaneEllipsoid
from diffsim.utils_render import init_viewer_ellipsoids
from diffsim.simulator import Simulator, SimulatorNode
from tqdm import trange, tqdm
from pathlib import Path
import os
import imageio
from utils.utils import make_video, createSoloModel

path_curr = Path().cwd()

rmodel, rgeomModel, rdata, rgeom_data = createSoloModel()


theta =  0# np.pi /6
c,s = np.cos(theta), np.sin(theta)
rty = np.array([[c,0.,s],
               [0.,1.,0.],
               [-s,0.,c]])
n = np.array([s,0.,c])
tx, ty =  np.array([c,0.,-s]), np.array([0.,1.,0.])
gpo = pin.SE3(rty,np.matrix([0.,0.,0.]).T)
q_init = pin.neutral(rmodel)
q_init[2] += .35
v_init = np.zeros(rmodel.nv)
pin.forwardKinematics(rmodel, rdata, q_init)
pin.updateGeometryPlacements(rmodel, rdata, rgeomModel, rgeom_data)

q0 = q_init
dq0 = pin.difference(rmodel, rmodel.qref, q0)
dqrobot0Ref = dq0[:rmodel.nv-6].copy()
v0 = np.zeros(rmodel.nv) 
x0 = np.zeros(2*rmodel.nv)
x0[:rmodel.nv] = dq0
x0[rmodel.nv:] = v0
dqtarget = torch.tensor(dq0)
dqtarget[2] += .1
dqtarget = dqtarget.detach().numpy()
vtarget = v0.copy()


coeff_friction = 0.7
coeff_rest = 0.0
dt = 5e-3


name = "solo_on_floor"
viz = init_viewer_ellipsoids(rmodel, rgeomModel, open=False)
viz.display(q_init)

## Making solo jump

T = 10

sim_nodes = [SimulatorNode( Simulator(rmodel, rgeomModel, dt, coeff_friction, coeff_rest, dt_collision=dt))]*T
N_iter = 100
ddl=8
u = (torch.ones((T,ddl))*0.).requires_grad_(True)
optimizer = torch.optim.Adam([u], lr = .05)
positions = []
forces_values = []
loss_values = []
contact_forces = np.zeros((T,))



torch.autograd.set_detect_anomaly(False)
N_samples, noise_intensity = 32, 1e-1
positions = np.zeros((N_iter,T,rmodel.nv))
forces_values = np.zeros((N_iter,T,ddl))
for i in trange(N_iter): 
    v, q = torch.tensor(v_init), torch.tensor(q_init)
    xnext = torch.cat((torch.tensor(dq0),torch.tensor(v0)), dim=0).detach().clone()
    for t in range(T):
        u_noise = torch.normal(torch.zeros(T,ddl,N_samples))
        xnext_int = torch.zeros_like(xnext)
        for N in range(N_samples):
            t_tau = torch.zeros_like(v)
            t_tau[6:] = u[t,:] +  noise_intensity* u_noise[t,:,N]
            xnext_sample = sim_nodes[t].makeStep(xnext, t_tau, calcPinDiff=True)
            xnext_int += xnext_sample
        xnext = xnext_int/N_samples
        positions[i,t,:] = xnext[:rmodel.nv].detach().numpy()
        forces_values[i,t,:] = u[t].detach().numpy()
    loss = .5*torch.square(xnext[2]- dqtarget[2]).sum() + 0.0*torch.square(xnext[8]- vtarget[2]).sum() + 1e-6*torch.square(u).sum()
    loss_values += [loss.detach().item()]
    #forces_values += [sum(u).detach().item()/.001]
    optimizer.zero_grad()
    loss.backward()
    #print(u.grad)
    optimizer.step()


plt.style.use('bmh')
plt.figure()
plt.plot([i for i in range(len(loss_values))], loss_values, label="loss")
plt.xlabel("Iterations")
plt.ylabel("")
plt.legend()
plt.show()

plt.figure()
for j in range(N_iter):
    plt.plot([i for i in range(forces_values.shape[1])], forces_values[j], alpha = .97**(N_iter-j), color = '#348ABD')
plt.xlabel("Iterations")
plt.ylabel("Force")
plt.show()

plt.figure()
for j in range(N_iter):
    plt.plot([i for i in range(positions.shape[1])], positions[j,:,2], alpha = .97**(N_iter-j), color = '#348ABD')
plt.xlabel("Time step")
plt.ylabel("Altitude")
plt.show()

traj = np.zeros((T,rmodel.nq))
contact_forces = []
xnext = torch.cat((torch.tensor(dq0),torch.tensor(v0)), dim=0).detach().clone()
u_init = (torch.ones((T,ddl))*0.)
u = torch.load(path_curr/'logs'/'solo_jumping'/'u.pt')
pattes_positions = []
acc_com = []
masstot = 0
for bodymass in rmodel.inertias.tolist():
    masstot += bodymass.mass
for t in range(T):
    t_tau = torch.zeros_like(v)
    t_tau[6:] = u[t]
    xnext = sim_nodes[t].makeStep(xnext, t_tau, calcPinDiff=True)
    pin.updateGeometryPlacements(rmodel, sim_nodes[t].rdata, rgeomModel, rgeom_data)
    traj[t,:] = pin.integrate(rmodel, rmodel.qref,xnext[:rmodel.nv].detach().numpy())
    contact_forces += [sim_nodes[t].rdata.contact_forces]
    acc_com += [sum(sim_nodes[t].rdata.contact_forces[:,2])/dt - masstot*9.81]
    pattes_positions += [np.array([rgeom_data.oMg[13].translation[2], rgeom_data.oMg[14].translation[2], rgeom_data.oMg[15].translation[2], rgeom_data.oMg[16].translation[2]])]


plt.figure()
plt.plot([i for i in range(traj.shape[0])], traj[:,2], color = '#348ABD')
plt.xlabel("Time step")
plt.ylabel("Altitude")
plt.show()

plt.figure()
for k in range(contact_forces[0].shape[0]):
    plt.plot([i for i in range(len(contact_forces))], [contact_force[k,2] for contact_force in contact_forces])
plt.xlabel("Time step")
plt.ylabel("contact forces")
plt.show()

plt.figure()
for k in range(pattes_positions[0].shape[0]):
    plt.plot([i for i in range(len(pattes_positions))], [patte_position[k]-0.03 for patte_position in pattes_positions])
plt.xlabel("Time step")
plt.ylabel("Positions of feet")
plt.show()

plt.figure()
plt.plot([i for i in range(len(acc_com))], acc_com, color = '#348ABD')
plt.xlabel("Time step")
plt.ylabel("Acc CoM")
plt.show()


viz.display(q_init)
time.sleep(3.)
for t in range(T):
    viz.display(traj[t])
    time.sleep(.2)


fps = 5.
os.makedirs(path_curr/'videos'/'solo_jumping', exist_ok=True)
make_video(viz, np.concatenate((np.expand_dims(q_init,axis=0),traj), axis = 0), path_curr/'videos'/'solo_jumping'/'RSGD.mp4', fps)


os.makedirs(path_curr/'logs'/'solo_jumping', exist_ok=True)
torch.save(u,path_curr/'logs'/'solo_jumping'/'u.pt')


## Using DDP with smoothed physics



class SmoothActionModel(croc.ActionModelAbstract):
    def __init__(self, state, simulator, jointCube, ptarget1 , cw1, cw2, dqrobot0Ref, noiseIntensity, nbSamples, terminal=False):
        self.sim = simulator
        self.ptarget1 = ptarget1
        self.terminal = terminal
        self.dqrobot0Ref = dqrobot0Ref
        self.t_dqrobot0Ref = torch.from_numpy(dqrobot0Ref)
        self.jointCube = jointCube
        self.costWeight1 = cw1
        self.costWeight2 = cw2
        self.costWeightAction = 1e-1
        self.costReg = 5e-7
        self.nv = self.sim.rmodel.nv 
        self.noiseIntensity = noiseIntensity
        self.nbSamples = nbSamples
        nu = 8
        nr = 1
        self.unone = np.zeros(self.nv)
        croc.ActionModelAbstract.__init__(self, state, nu, nr)

    def createData(self):
        data = croc.ActionModelAbstract.createData(self)
        data.node = SimulatorNode(self.sim)
        return data
    
    def calc(self, data, x, u=None, uNoise = None):
        if u is None:
            u = self.unone
        if uNoise is None:
            uNoise = torch.zeros((self.nbSamples, u.shape[0]))
        t_x = torch.from_numpy(x)
        t_xnext = torch.zeros_like(t_x)
        for ui in uNoise:
            t_tau = torch.zeros(self.nv)
            t_tau[6:] = u[0]+ ui
            t_xnexti = data.node.makeStep(t_x, t_tau, calcPinDiff=False)
            t_xnext += t_xnexti
        t_xnext /= self.nbSamples
        if self.terminal:
            x_tar = torch.zeros_like(t_xnext)
            x_tar[2] = self.ptarget1[2]
            t_res1 = self.costWeight1*(t_xnext - x_tar)
        else:
            t_res1 = torch.zeros_like(t_xnext)
        data.xnext[:] = t_xnext.detach().numpy()
        data.r[0] = self.costWeight1 * (t_xnext.detach().numpy()[2] - self.ptarget1[2])
        data.cost =  0.5 * sum(data.r**2)
        return data.xnext, data.cost


    def calcDiff(self, data, x, u=None):
        if u is None:
            u = self.unone
        xnext, cost = self.calc(data, x, u)
        t_x = torch.from_numpy(x).requires_grad_(True)
        t_u = torch.from_numpy(u).requires_grad_(True)
        def calcTorch(t_x, t_u):
            t_xnext = torch.zeros_like(t_x)
            uNoise = self.noiseIntensity*torch.randn((self.nbSamples, t_u.size()[0]))
            for i in range(self.nbSamples): 
                t_tau = torch.zeros(self.nv) 
                t_tau[2] = t_u[0] + uNoise[i]
                t_xnexti = data.node.makeStep(t_x, t_tau, calcPinDiff=True)
                t_xnext += t_xnexti
            t_xnext /= self.nbSamples 
            if self.terminal:
                x_tar = torch.zeros_like(t_xnext)
                x_tar[2] = self.ptarget1[2]
                t_res1 = self.costWeight1*(t_xnext - x_tar)
            else:
                t_res1 = self.costWeight1 *torch.zeros_like(t_xnext)
            t_res_reg =  torch.tensor([0.]) 
            t_res_u = self.costWeight2*self.costWeightAction * t_u
            t_res = torch.cat([t_res1, t_res_u, t_res_reg])
            t_cost = 0.5 * torch.sum(t_res * t_res)
            return t_xnext, t_cost
        
        J = torch.autograd.functional.jacobian(calcTorch, (t_x, t_u))
        data.Fx[:] = J[0][0].numpy()
        data.Fu[:] = J[0][1][:,0].numpy()
        if np.linalg.norm(data.Fu[:])<1e-9:
            data.Fu[:] = 0
        data.Lx[:] = J[1][0].numpy()
        data.Lu[:] = J[1][1].numpy()
        return xnext, cost


robotModel = rmodel.copy()
robotData = robotModel.createData()
q = q0
T = 10
dt = 5e-3
state = croc.StateVector(2*rmodel.nv)
ug = pin.computeGeneralizedGravity(robotModel, robotData, q)
jointBaseId = 1
sim = Simulator(rmodel, rgeomModel, dt, coeff_friction, coeff_rest, dt_collision=dt, steps_contact_cascade=10)
actionModel1 = SmoothActionModel(state, sim, jointBaseId, dqtarget[:3], 0, 0., dqrobot0Ref, 1e-2, 8, terminal=False)
actionModelTerminal = SmoothActionModel(state, sim, jointBaseId, dqtarget[:3], 1000., 1000., dqrobot0Ref,10e-0, 8, terminal=True)
problem = croc.ShootingProblem(x0, [actionModel1]*T,
                               actionModelTerminal)

ddp = croc.SolverFDDP(problem)
log = croc.CallbackLogger()
ddp.setCallbacks([log, croc.CallbackVerbose()])


init_us = [ug[2:3]*0.] * T
init_xs = problem.rollout(init_us)
done = ddp.solve(init_xs, init_us, 10)
print(done)


rmodel.joints[1]