#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  7 14:44:00 2021

@author: qlelidec
"""

import pinocchio as pin
import numpy as np
import hppfcl
from tqdm import trange
import torch
from diffsim.simulator import Simulator, SimulatorNode
from diffsim.shapes import Ellipsoid, Plane
from diffsim.costs import residualTranslation
#import crocoddyl as croc
from diffsim.shapes import Ellipsoid, Plane 
from pinocchio.visualize import MeshcatVisualizer
from diffsim.collision_pairs import CollisionPairPlaneEllipsoid
from diffsim.utils_render import init_viewer_ellipsoids
import time

model = pin.Model()
freeflyer = pin.JointModelFreeFlyer()
joint_id = model.addJoint(0, freeflyer, pin.SE3.Identity(), "joint1")
M = pin.SE3(np.eye(3), np.matrix([0., 0., 0.]).T)
model.appendBodyToJoint(joint_id, pin.Inertia.FromBox(1,0.2,0.2,0.2), M)
data = model.createData()

geom_model = pin.GeometryModel()
cube = hppfcl.Box(0.2,0.2,0.2)
floor = hppfcl.Box(5.,.5,0.1)
geom_cube = pin.GeometryObject("cube", 0, joint_id, cube, pin.SE3.Identity())
theta =  0# np.pi /6
c,s = np.cos(theta), np.sin(theta)
rty = np.array([[c,0.,s],
               [0.,1.,0.],
               [-s,0.,c]])
n = np.array([s,0.,c])
tx, ty =  np.array([c,0.,-s]), np.array([0.,1.,0.])
gpo = pin.SE3(rty,np.matrix([0.,0.,0.]).T)
geom_floor = pin.GeometryObject("floor",0,0, floor,gpo)
geom_cube.meshColor = np.array([1.0, 0.2, 0.2, 1.])
geom_floor.meshColor = np.array([0.2, 0.2, 1., 1.])
geom_model.addGeometryObject(geom_cube)
geom_model.addGeometryObject(geom_floor)
col_pair = pin.CollisionPair(0,1)
geom_model.addCollisionPair(col_pair)
geom_data = geom_model.createData()

q_init = pin.SE3ToXYZQUAT(gpo)
q_init[:3] += .15*n
v_init = np.zeros(model.nv)
pin.forwardKinematics(model, data, q_init)
pin.updateGeometryPlacements(model, data, geom_model, geom_data)

rmodel = pin.Model()
freeflyer = pin.JointModelFreeFlyer()
jointCube = rmodel.addJoint(0, freeflyer, pin.SE3.Identity(), "joint1")
M = pin.SE3(np.eye(3), np.matrix([0., 0., 0.]).T)
rmodel.appendBodyToJoint(jointCube, pin.Inertia.FromBox(1,0.2,0.2,0.2), M)
data = rmodel.createData()

rgeomModel = pin.GeometryModel()
a = 0.2
r = np.array([a/4, a/4, a/4])
cube_shape = hppfcl.Box(a, a, a)
rgeomModel.computeColPairDist = []

n = np.array([0., 0., 1])
p = np.array([0., 0., 0.0])
h = np.array([10., 10., 0.1])
plane_shape = Plane(0, 'plane', n, p, h)
T = pin.SE3(plane_shape.R, plane_shape.t)
plane = pin.GeometryObject("plane", 0, 0, plane_shape, T)
plane.meshColor = np.array([0.5, 0.5, 0.5, 1.]) 
planeId = rgeomModel.addGeometryObject(plane)

# add balls to cube

n = np.array([0., 0., 1])
p = np.array([0., 0., 0.0])
h = np.array([10., 10., 0.01])
plane_shape = Plane(0, 'plane', n, p, h)
T = pin.SE3(plane_shape.R, plane_shape.t)
plane = pin.GeometryObject("plane", 0, 0, plane_shape, T)
plane.meshColor = np.array([0.5, 0.5, 0.5, 1.]) 
planeId = rgeomModel.addGeometryObject(plane)

rgeomModel.collision_pairs = []

R = pin.utils.eye(3)
t = np.matrix([a/4, -a/4, -a/4]).T
ball_shape = Ellipsoid(jointCube, 'ball', r,pin.SE3(R, t)) 
geom_ball1 = pin.GeometryObject("ball1", jointCube, jointCube, ball_shape, pin.SE3(R, t)) 
geom_ball1.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
ball1Id = rgeomModel.addGeometryObject(geom_ball1)
#col_pair1 = pin.CollisionPair(ball1Id, planeId)
#rgeomModel.addCollisionPair(col_pair1)
#rgeomModel.computeColPairDist.append(False)
col_pair1 = CollisionPairPlaneEllipsoid(planeId, ball1Id)
rgeomModel.collision_pairs += [col_pair1]
rgeomModel.computeColPairDist.append(False)

R = pin.utils.eye(3)
t = np.matrix([a/4, a/4, -a/4]).T
ball_shape = Ellipsoid(jointCube, 'ball', r,pin.SE3(R, t)) 
geom_ball2 = pin.GeometryObject("ball2", jointCube, jointCube, ball_shape, pin.SE3(R, t)) 
geom_ball2.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
ball2Id = rgeomModel.addGeometryObject(geom_ball2)
#col_pair2 = pin.CollisionPair(ball2Id, planeId)
#rgeomModel.addCollisionPair(col_pair2)
#rgeomModel.computeColPairDist.append(False)
col_pair2 = CollisionPairPlaneEllipsoid(planeId, ball2Id)
rgeomModel.collision_pairs += [col_pair2]
rgeomModel.computeColPairDist.append(False)

R = pin.utils.eye(3)
t = np.matrix([a/4, a/4, a/4]).T
ball_shape = Ellipsoid(jointCube, 'ball', r,pin.SE3(R, t)) 
geom_ball3 = pin.GeometryObject("ball3", jointCube, jointCube, ball_shape, pin.SE3(R, t)) 
geom_ball3.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
ball3Id = rgeomModel.addGeometryObject(geom_ball3)
#col_pair3 = pin.CollisionPair(ball3Id, planeId)
#rgeomModel.addCollisionPair(col_pair3)
#rgeomModel.computeColPairDist.append(False)
col_pair3 = CollisionPairPlaneEllipsoid(planeId, ball3Id)
rgeomModel.collision_pairs += [col_pair3]
rgeomModel.computeColPairDist.append(False)

R = pin.utils.eye(3)
t = np.matrix([a/4, -a/4, a/4]).T
ball_shape = Ellipsoid(jointCube, 'ball', r,pin.SE3(R, t)) 
geom_ball4 = pin.GeometryObject("ball4", jointCube, jointCube, ball_shape, pin.SE3(R, t)) 
geom_ball4.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
ball4Id = rgeomModel.addGeometryObject(geom_ball4)
#col_pair4 = pin.CollisionPair(ball4Id, planeId)
#rgeomModel.addCollisionPair(col_pair4)
#rgeomModel.computeColPairDist.append(False)
col_pair4 = CollisionPairPlaneEllipsoid(planeId, ball4Id)
rgeomModel.collision_pairs += [col_pair4]
rgeomModel.computeColPairDist.append(False)

R = pin.utils.eye(3)
t = np.matrix([-a/4, -a/4, -a/4]).T
ball_shape = Ellipsoid(jointCube, 'ball', r,pin.SE3(R, t)) 
geom_ball5 = pin.GeometryObject("ball5", jointCube, jointCube, ball_shape, pin.SE3(R, t)) 
geom_ball5.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
ball5Id = rgeomModel.addGeometryObject(geom_ball5)
#col_pair5 = pin.CollisionPair(ball5Id, planeId)
#rgeomModel.addCollisionPair(col_pair5)
#rgeomModel.computeColPairDist.append(False)
col_pair5 = CollisionPairPlaneEllipsoid(planeId, ball5Id)
rgeomModel.collision_pairs += [col_pair5]
rgeomModel.computeColPairDist.append(False)

R = pin.utils.eye(3)
t = np.matrix([-a/4, a/4, -a/4]).T
ball_shape = Ellipsoid(jointCube, 'ball', r,pin.SE3(R, t)) 
geom_ball6 = pin.GeometryObject("ball6", jointCube, jointCube, ball_shape, pin.SE3(R, t)) 
geom_ball6.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
ball6Id = rgeomModel.addGeometryObject(geom_ball6)
#col_pair6 = pin.CollisionPair(ball6Id, planeId)
#rgeomModel.addCollisionPair(col_pair6)
#rgeomModel.computeColPairDist.append(False)
col_pair6 = CollisionPairPlaneEllipsoid(planeId, ball6Id)
rgeomModel.collision_pairs += [col_pair6]
rgeomModel.computeColPairDist.append(False)

R = pin.utils.eye(3)
t = np.matrix([-a/4, a/4, a/4]).T
ball_shape = Ellipsoid(jointCube, 'ball', r,pin.SE3(R, t)) 
geom_ball7 = pin.GeometryObject("ball7", jointCube, jointCube, ball_shape, pin.SE3(R, t)) 
geom_ball7.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
ball7Id = rgeomModel.addGeometryObject(geom_ball7)
#col_pair7 = pin.CollisionPair(ball7Id, planeId)
#rgeomModel.addCollisionPair(col_pair7)
#rgeomModel.computeColPairDist.append(False)
col_pair7 = CollisionPairPlaneEllipsoid(planeId, ball7Id)
rgeomModel.collision_pairs += [col_pair7]
rgeomModel.computeColPairDist.append(False)

R = pin.utils.eye(3)
t = np.matrix([-a/4, -a/4, a/4]).T
ball_shape = Ellipsoid(jointCube, 'ball', r,pin.SE3(R, t)) 
geom_ball8 = pin.GeometryObject("ball8", jointCube, jointCube, ball_shape, pin.SE3(R, t)) 
geom_ball8.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
ball8Id = rgeomModel.addGeometryObject(geom_ball8)
#col_pair8 = pin.CollisionPair(ball8Id, planeId)
#rgeomModel.addCollisionPair(col_pair8)
#rgeomModel.computeColPairDist.append(False)
col_pair8 = CollisionPairPlaneEllipsoid(planeId, ball8Id)
rgeomModel.collision_pairs += [col_pair8]
rgeomModel.computeColPairDist.append(False)

rgeom_data = rgeomModel.createData()

coeff_friction = 0.15
coeff_rest = 0.0
dt = 5e-3
sim = SimulatorNode(Simulator(rmodel, rgeomModel, dt, coeff_friction, coeff_rest, dt_collision=dt))
#state = croc.StateVector(2*rmodel.nv)

theta =  0# np.pi /6
c,s = np.cos(theta), np.sin(theta)
rty = np.array([[c,0.,s],
               [0.,1.,0.],
               [-s,0.,c]])
n = np.array([s,0.,c])
tx, ty =  np.array([c,0.,-s]), np.array([0.,1.,0.])
gpo = pin.SE3(rty,np.matrix([0.,0.,0.]).T)
q_init = pin.SE3ToXYZQUAT(gpo)
q_init[:3] += .1*n
v_init = np.zeros(rmodel.nv)
pin.forwardKinematics(rmodel, data, q_init)
pin.updateGeometryPlacements(rmodel, data, rgeomModel, rgeom_data)

q0 = q_init
dq0 = pin.difference(rmodel, rmodel.qref, q0)
qrobot0Ref = dq0[:rmodel.nv-6].copy()
dq0 = pin.difference(rmodel, rmodel.qref, q0)
dqrobot0Ref = dq0[:rmodel.nv-6].copy()
v0 = np.zeros(rmodel.nv) 
#v0[rmodel.nv-6:] = np.zeros(6)
x0 = np.zeros(2*rmodel.nv)
x0[:rmodel.nv] = dq0
x0[rmodel.nv:] = v0
dqtarget = torch.tensor(dq0)
dqtarget[2] += .1
dqtarget = dqtarget.detach().numpy()
vtarget = v0.copy()

T = 10

sim_nodes = [SimulatorNode( Simulator(rmodel, rgeomModel, dt, coeff_friction, coeff_rest, dt_collision=dt))]*T
dqtarget = torch.tensor(dq0)
dqtarget[0] += .1
dqtarget = dqtarget.detach().numpy()
vtarget = v0.copy()
u = (torch.ones(T)*.01).requires_grad_(True)
optimizer = torch.optim.Adam([u], lr = 4.)
positions = []
forces_values = []
loss_values = []

torch.autograd.set_detect_anomaly(True)
N_iter = 100
N_samples, noise_intensity = 1, 0e-1
for i in trange(N_iter): 
    #sim_nodes = [SimulatorNode( Simulator(rmodel, rgeomModel, dt, coeff_friction, coeff_rest, dt_collision=dt))]*T
    v, q = torch.tensor(v_init), torch.tensor(q_init)
    xnext = torch.cat((torch.tensor(dq0),torch.tensor(v0)), dim=0).detach().clone()
    positions += [[]]
    forces_values += [[]]
    for t in range(T):
        u_noise = torch.normal(torch.zeros(T,N_samples))
        xnext_int = torch.zeros_like(xnext)
        for N in range(N_samples):
            t_tau = torch.zeros_like(v)
            t_tau[0] = u[t] +  noise_intensity* u_noise[t,N]
            xnext_sample = sim_nodes[t].makeStep(xnext, t_tau, calcPinDiff=True)
            xnext_int += xnext_sample
        xnext = xnext_int/N_samples
        positions[-1] += [xnext[0].detach().item()]
        forces_values[-1] += [u[t].detach().item()]
    loss = .5*torch.square(xnext[0]- dqtarget[0]).sum() + .0005*torch.square(xnext[6]- vtarget[0]).sum()
    loss_values += [loss.detach().item()]
    #forces_values += [sum(u).detach().item()/.001]
    optimizer.zero_grad()
    loss.backward()
    print("x", xnext)
    print("u", u)
    print(u.grad)
    optimizer.step()