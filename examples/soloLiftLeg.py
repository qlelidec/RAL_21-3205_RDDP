#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  4 11:46:37 2021

@author: qlelidec
"""

import pinocchio as pin
import numpy as np
import torch
from torch.autograd.functional import jacobian
import matplotlib.pyplot as plt
from diffsim.shapes import Ellipsoid, Plane 
from pinocchio.visualize import MeshcatVisualizer
from diffsim.collision_pairs import CollisionPairPlaneEllipsoid
from diffsim.utils_render import init_viewer_ellipsoids
from diffsim.simulator import Simulator, SimulatorNode
from tqdm import trange
from pathlib import Path
import os
from utils.utils import make_video, createSoloModel
import argparse


path_curr = Path().cwd()

EXP_TYPE = "lift_leg"

default_params = {"solo":{"lr":.5,"n_iter":20,"DT":5e-3, "T":20, "nb_samples":4,"noise_intensity":7e-1, "optimizer":"SGD", "adapt_smoothing": "None", "save_path":path_curr/'logs'/'solo_lifting_leg_test'}}
EXP_ID = 0
LEARNING_RATE = default_params["solo"]["lr"]
NUM_ITER = default_params["solo"]["n_iter"]
OPTIMIZER = default_params["solo"]["optimizer"]
DT = default_params["solo"]["DT"]
TIME_HORIZON = default_params["solo"]["T"]
NB_SAMPLES = default_params["solo"]["nb_samples"]
NOISE_INTENSITY = default_params["solo"]["noise_intensity"]
ADAPTIVE_SMOOTHING = default_params["solo"]["adapt_smoothing"]
SAVE_PATH = default_params["solo"]["save_path"]

parser = argparse.ArgumentParser()
parser.add_argument('-et', '--experiment-type', type=str, default=EXP_TYPE)
parser.add_argument('-eid', '--experiment-id', type=int, default=EXP_ID)
parser.add_argument('-lr', '--learning-rate', type=float, default=LEARNING_RATE) 
parser.add_argument('-ni', '--num-iter', type=int, default=NUM_ITER)
parser.add_argument('-opt', '--optimizer', type=str, default=OPTIMIZER)
parser.add_argument('-nbs', '--nb-samples', type=float, default=NB_SAMPLES) 
parser.add_argument('-noi', '--noise-intensity', type=float, default=NOISE_INTENSITY) 
parser.add_argument('-T', '--time-horizon', type=int, default=TIME_HORIZON) 
parser.add_argument('-dt', '--time-step', type=float, default=DT) 
parser.add_argument('-ada', '--adaptive-smoothing', type=str, default=ADAPTIVE_SMOOTHING) 
parser.add_argument('-svp', '--save-path', type=str, default=SAVE_PATH) 



args = parser.parse_args()

rmodel, rgeomModel, rdata, rgeom_data = createSoloModel()


#theta =  0# np.pi /6
#c,s = np.cos(theta), np.sin(theta)
#rty = np.array([[c,0.,s],
#               [0.,1.,0.],
#               [-s,0.,c]])
#n = np.array([s,0.,c])
#tx, ty =  np.array([c,0.,-s]), np.array([0.,1.,0.])
#gpo = pin.SE3(rty,np.matrix([0.,0.,0.]).T)
q_init = rmodel.qref

v_init = np.zeros(rmodel.nv)
pin.forwardKinematics(rmodel, rdata, q_init)
pin.updateGeometryPlacements(rmodel, rdata, rgeomModel, rgeom_data)

q0 = q_init.copy()
q_target = q_init.copy()
q_target[-1] += .9
dq0 = pin.difference(rmodel, rmodel.qref, q0)
dqrobot0Ref = dq0[:rmodel.nv-6].copy()
v0 = np.zeros(rmodel.nv)
x0 = np.zeros(2*rmodel.nv)
x0[:rmodel.nv] = dq0
x0[rmodel.nv:] = v0
dqtarget = torch.from_numpy(pin.difference(rmodel, rmodel.qref, q_target))
vtarget = v0.copy()

coeff_friction = 0.7
coeff_rest = 0.0
dt = args.time_step
T = args.time_horizon

sim_nodes = [SimulatorNode( Simulator(rmodel, rgeomModel, dt, coeff_friction, coeff_rest, steps_contact_cascade = 20, dt_collision=dt, k_baumgarte=.1))]*T
sim_nodes2 = [SimulatorNode( Simulator(rmodel, rgeomModel, dt, coeff_friction, coeff_rest, steps_contact_cascade = 10, dt_collision=dt, k_baumgarte=.1))]*T
N_iter = args.num_iter
ddl=rmodel.nv-6
adaptive_smoothing = args.adaptive_smoothing
theta_ad = .001
N_samples, noise_intensity = args.nb_samples, args.noise_intensity
alpha = args.learning_rate

u_init = np.load("utils/solo12_quasistatic_u.npy")
u_init = torch.tensor(np.tile(u_init,(T,1))).requires_grad_(False)

u = u_init.clone().requires_grad_(True)

#optimizer = torch.optim.Adam([u], lr = .01 )#betas=(1*0.9,1*0.999))
optimizer = torch.optim.SGD([u], lr = 0.005, momentum=0.9*0.)

loss_values = []
noise_intensities = []
N_samples_values = []
contact_forces = np.zeros((T,))
positions = np.zeros((N_iter,T,rmodel.nv))
forces_values = np.zeros((N_iter,T,ddl))
gradient_values = np.zeros((N_iter, T, ddl))

def loss_function(rmodel, xnext, u, N_samples, noise_intensity, u_noise=None, adaptive_estimate = False):
    grads_samples = torch.zeros(T,N_samples, 2*rmodel.nv+rmodel.nv, 2*rmodel.nv) if adaptive_estimate else None
    for t in range(T):
        if u_noise is None:
            u_noise = torch.normal(torch.zeros(T,ddl,N_samples))
        xnext_int = torch.zeros_like(xnext)
        for N in range(N_samples):
            t_tau = torch.zeros(rmodel.nv)
            t_tau[6:] = u[t,:] +  noise_intensity* u_noise[t,:,N]
            xnext_sample = sim_nodes[t].makeStep(xnext, t_tau, calcPinDiff=True)
            if adaptive_estimate:
                inputs = (xnext.detach(), t_tau.detach())
                jaco = jacobian(lambda xnext, t_tau: sim_nodes2[t].makeStep(xnext, t_tau, calcPinDiff=True), inputs)
                grads_samples[t,N,:2*rmodel.nv,:] = jaco[0][0]
                grads_samples[t,N,2*rmodel.nv:,:] = jaco[0][1]
            xnext_int += xnext_sample
        xnext = xnext_int/N_samples
        positions[i,t,:] = xnext[:rmodel.nv].detach().numpy()
        forces_values[i,t,:] = u[t].detach().numpy()
    loss = 7*torch.square(xnext[:rmodel.nv]- dqtarget).sum() + 1e-7*torch.square(u - u_init).sum()
    return loss, u_noise, grads_samples

torch.autograd.set_detect_anomaly(False)
for i in trange(N_iter): 
    v, q = torch.tensor(v_init), torch.tensor(q_init)
    xnext = torch.cat((torch.tensor(dq0),torch.tensor(v0)), dim=0).detach().clone()
    loss, u_noise, grads_samples = loss_function(rmodel, xnext, u, N_samples, noise_intensity, adaptive_estimate = adaptive_smoothing)
    loss_values += [loss.detach().item()]
    optimizer.zero_grad()
    loss.backward()
    gamma = 2.
    gradient_values[i,:,:] = u.grad.detach().numpy()
    noise_intensities += [noise_intensity]
    N_samples_values += [N_samples]
    alpha *= (gamma**(1/2))
    print("current loss", loss_values[-1])
    while True:
        try_loss, _, _ = loss_function(rmodel, xnext, u - alpha*u.grad, N_samples, noise_intensity, u_noise)
        print("try", try_loss.detach().item())
        print(loss - try_loss, ">", alpha *.1 *torch.square(u.grad).sum())
        if loss - try_loss > alpha *.001 *torch.square(u.grad).sum():
            u.data  = u.data - alpha*u.grad.detach()
            break
        else:
            alpha *= .6
    if adaptive_smoothing=="adaptive":
        #print("grad", grads_samples.size(), grads_samples[10,:,:,:].mean(), grads_samples[10,1,:,:].mean())
        grad_var, grad_mean = torch.var_mean(grads_samples, dim = 1)
        
        #grad_mean = torch.cat((grad_mean[:,:2*rmodel.nv,:],grad_mean[:,2*rmodel.nv+6:,:]),dim = 1)
        grad_mean =grad_mean[:,2*rmodel.nv+6:,:]
        print("mean", grad_mean[:,:,:].mean())
        print(grad_mean[0,:,:])
        grad_mean = torch.square(grad_mean).sum(dim=(-1,-2))
        #grad_var = torch.cat((grad_var[:,:2*rmodel.nv,:], grad_var[:,2*rmodel.nv+6:,:] ), dim =1 )
        grad_var = grad_var[:,2*rmodel.nv+6:,:]
        print("var", grad_var.size(),grad_var[:,:,:].mean())
        print(grad_var[0,:,:])
        grad_var = torch.abs(grad_var).sum(dim = (-1,-2))
        print("mean", grad_mean)
        print("var", grad_var)
        print("adapt", grad_var/N_samples, (theta_ad**2)*grad_mean)
        if torch.any(torch.isnan(grad_var)):
            print(torch.any(grads_samples).isnan(), N_samples)
        if torch.all(grad_var/N_samples > (theta_ad**2)*grad_mean):
            noise_intensity *= .5
        elif torch.all(grad_var/N_samples < (.0005**2)*grad_mean):
            N_samples = max(int(N_samples/2),1)
    elif adaptive_smoothing == "decreasing":
        noise_intensity *= (i+1)/(i+2)
    
        
    #optimizer.step()
    
os.makedirs(args.save_path, exist_ok=True)
torch.save(u,args.save_path/'u.pt')

plt.style.use('bmh')
plt.figure()
plt.plot([i for i in range(len(loss_values))], loss_values)
plt.xlabel("Iterations")
plt.ylabel("Loss")
plt.savefig(args.save_path/'loss_values.pdf')

plt.figure()
plt.plot([i for i in range(len(noise_intensities))], noise_intensities)
plt.xlabel("Iterations")
plt.ylabel("Noise intensity")
plt.savefig(args.save_path/'noiseIntensities_values.pdf')

plt.figure()
plt.plot([i for i in range(len(N_samples_values))], N_samples_values)
plt.xlabel("Iterations")
plt.ylabel("Nb of samples")
plt.savefig(args.save_path/'Nbsamples_values.pdf')

plt.figure()
plt.semilogy([i for i in range(gradient_values.shape[0])], np.sum(np.square(gradient_values), axis =(1,2)))
plt.xlabel("Iterations")
plt.ylabel("Gradients")
plt.savefig(args.save_path/'gradient_values.pdf')

plt.figure()
for j in range(N_iter):
    plt.plot([i for i in range(forces_values.shape[1])], forces_values[j], alpha = .97**(N_iter-j), color = '#348ABD')
plt.xlabel("Iterations")
plt.ylabel("Force")
plt.savefig(args.save_path/'force_values.pdf')

plt.figure()
for j in range(N_iter):plt.plot([i for i in range(positions.shape[1])], positions[j,:,2], alpha = .97**(N_iter-j), color = '#348ABD')
plt.xlabel("Time step")
plt.ylabel("Altitude")
plt.savefig(args.save_path/'positions_values.pdf')

traj = np.zeros((T+1,rmodel.nq))
contact_forces = []
xnext = torch.cat((torch.tensor(dq0),torch.tensor(v0)), dim=0).detach().clone()
pattes_positions = []
acc_com = []
masstot = 0
for bodymass in rmodel.inertias.tolist():
    masstot += bodymass.mass
traj[0,:] = rmodel.qref
for t in range(T):
    t_tau = torch.zeros(rmodel.nv)
    t_tau[6:] = u[t]
    xnext = sim_nodes[t].makeStep(xnext, t_tau, calcPinDiff=False)
    pin.updateGeometryPlacements(rmodel, sim_nodes[t].rdata, rgeomModel, rgeom_data)
    traj[t+1,:] = pin.integrate(rmodel, rmodel.qref,xnext[:rmodel.nv].detach().numpy())
    contact_forces += [sim_nodes[t].rdata.contact_forces]
    acc_com += [sum(sim_nodes[t].rdata.contact_forces[:,2])/(masstot*dt) - 9.81]
    pattes_positions += [np.array([rgeom_data.oMg[-1].translation[2], rgeom_data.oMg[-2].translation[2], rgeom_data.oMg[-3].translation[2], rgeom_data.oMg[-4].translation[2]])]
    
plt.figure()
plt.plot([i for i in range(traj.shape[0])], traj[:,2], color = '#348ABD')
plt.xlabel("Time step")
plt.ylabel("Altitude")

plt.figure()
for k in range(contact_forces[0].shape[0]):
    plt.plot([i for i in range(len(contact_forces))], [contact_force[k,2] for contact_force in contact_forces])
plt.xlabel("Time step")
plt.ylabel("contact forces")
plt.savefig(args.save_path/'contact_forces_values.pdf')

plt.figure()
for k in range(pattes_positions[0].shape[0]):
    plt.plot([i for i in range(len(pattes_positions))], [patte_position[k] for patte_position in pattes_positions])
plt.xlabel("Time step")
plt.ylabel("Positions of feet")
plt.savefig(args.save_path/'feet_values.pdf')

plt.figure()
plt.plot([i for i in range(len(acc_com))], acc_com, color = '#348ABD')
plt.xlabel("Time step")
plt.ylabel("Acc CoM (m/s^2)")
plt.savefig(args.save_path/'CoMacc_values.pdf')

name = "solo_lift_leg"
viz_lift = init_viewer_ellipsoids(rmodel, rgeomModel, open=False)
fps = 5.
os.makedirs(args.save_path, exist_ok=True)
make_video(viz_lift, np.concatenate((np.expand_dims(q_init,axis=0),traj), axis = 0), args.save_path/'ARS-GD-LS.mp4', fps)
