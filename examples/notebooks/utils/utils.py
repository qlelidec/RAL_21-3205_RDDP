#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  4 11:02:14 2021

@author: qlelidec
"""

import pinocchio as pin
import numpy as np
from tqdm import trange
import imageio
import os
from pathlib import Path
from tqdm import tqdm
import torch
from qcqp import BoxQPFn2, SignedBoxQPFn2
import hppfcl as fcl 
import example_robot_data
from diffsim.shapes import Ellipsoid, Plane 
from diffsim.collision_pairs import CollisionPairPlaneEllipsoid
import crocoddyl as croc
from diffsim.simulator import SimulatorNode
import example_robot_data as erd
from multiprocessing import Pool

def make_video(viz, positions, name, fps):
    imgs = []
    print("generating frames")
    for q_i in tqdm(positions):
        viz.display(q_i)
        img_i = np.array(viz.viewer.get_image())
        imgs += [img_i]
    
    writer = imageio.get_writer(name, fps=fps)
    for im in imgs:
        writer.append_data(im)
    writer.close()
    return 

def make_capture(viz, position, name):
    print("generating capture")
    viz.display(position)
    capture = viz.viewer.get_image()
    capture.save(name)
    return 

def RK4Update(model,data,q,v,u,dt):
    a1 = pin.aba(model,data,q,v,u)
    q1,v1 = pin.integrate(model, q,v*dt/2), v+a1*dt/2
    a2 = pin.aba(model,data,q1,v1,u)
    q2,v2 = pin.integrate(model, q,v1*dt/2), v+a2*dt/2
    a3 = pin.aba(model,data,q2,v2,u)
    q3,v3 = pin.integrate(model, q,v2*dt), v+a3*dt
    a4 = pin.aba(model,data,q3,v3,u)
    return pin.integrate(model, q,(dt/3)*((1/2)*v+v1+v2+(1/2)*v3)), v+ (dt/3)*((1/2)*a1 + a2 + a3 + (1/2)*a4)

def EulerUpdate(model,data,q,v,u,dt):
    a1 = pin.aba(model,data,q,v,u)
    return pin.integrate(model, q,v*dt), v+a1*dt

def aba_with_friction(model,data,q,v,u,cf,dt):
    a = pin.aba(model,data,q,v,u)
    Minv = np.linalg.inv(pin.crba(model,data,q))
    af = np.where(np.abs(v) < 1e-3, 
                  np.sign(a)*np.clip(np.abs(a)-np.abs(np.dot(Minv,np.sign(a)*cf)),0,None),
                  a-np.dot(Minv,np.sign(v)*cf))
    vnew = v + dt*af
    return vnew, [0]

def aba_with_friction2(model,data,q,v,u,cf,dt): #impulse formulation
    a = pin.aba(model,data,q,v,u)
    vfree = v + a*dt
    Minv = np.linalg.inv(pin.crba(model,data,q))
    P = dt*torch.from_numpy(Minv).unsqueeze(0)
    p = torch.from_numpy(vfree).unsqueeze(0).unsqueeze(-1)
    l_min = -torch.from_numpy(cf).unsqueeze(0).unsqueeze(-1)
    l_max = torch.from_numpy(cf).unsqueeze(0).unsqueeze(-1)
    warm_start = torch.zeros_like(l_min)
    eps, max_iter = 1e-8, 100
    tau_f = BoxQPFn2().apply(P,p,l_min,l_max, warm_start, eps, max_iter).detach().squeeze(0).squeeze(-1).numpy()
    vnew = vfree + np.dot(Minv,tau_f)*dt
    return vnew, tau_f

def aba_with_friction3(model,data,q,v,u,cf,dt): # acc formulation wo power constraint
    a = pin.aba(model,data,q,v,u)
    Minv = np.linalg.inv(pin.crba(model,data,q))
    P = dt*torch.from_numpy(Minv).unsqueeze(0)
    p = torch.from_numpy(a).unsqueeze(0).unsqueeze(-1)
    l_min = -torch.from_numpy(cf).unsqueeze(0).unsqueeze(-1)
    l_max = torch.from_numpy(cf).unsqueeze(0).unsqueeze(-1)
    warm_start = torch.zeros_like(l_min)
    eps, max_iter = 1e-8, 100
    tau_f = BoxQPFn2().apply(P,p,l_min,l_max, warm_start, eps, max_iter).detach().squeeze(0).squeeze(-1).numpy()
    vnew = v + dt*(a + np.dot(Minv,tau_f))
    return vnew, tau_f

def aba_with_friction4(model,data,q,v,u,cf,dt): # acc formulation with power constraint
    a = pin.aba(model,data,q,v,u)
    Minv = np.linalg.inv(pin.crba(model,data,q))
    P = torch.from_numpy(Minv).unsqueeze(0)
    p = torch.from_numpy(a).unsqueeze(0).unsqueeze(-1)
    vt = torch.from_numpy(v).unsqueeze(0).unsqueeze(-1)
    l_min = -torch.from_numpy(cf).unsqueeze(0).unsqueeze(-1)
    l_max = torch.from_numpy(cf).unsqueeze(0).unsqueeze(-1)
    #warm_start = torch.zeros_like(l_min)
    warm_start = torch.where(vt<0,l_max,l_min)
    eps, max_iter = 1e-8, 100
    tau_f = SignedBoxQPFn2().apply(P,p,l_min,l_max, vt, warm_start, eps, max_iter).detach().squeeze(0).squeeze(-1).numpy()
    #print(P,q,l_min,l_max, vt)
    vnew = v + dt*(a + np.dot(Minv,tau_f))
    #if np.any(tau_f==0):
    #    print(P,p,l_min,l_max, vt,q)
    return vnew, tau_f

def aba_with_friction5(model,data,q,v,u,cf,dt):
    a = pin.aba(model,data,q,v,u)
    Minv = np.linalg.inv(pin.crba(model,data,q))
    af = np.where(np.abs(v) < 1e-3, 
                  np.sign(a)*np.clip(np.abs(a)-np.abs(np.dot(Minv,np.sign(a)*cf)),0,None),
                  a-np.dot(Minv,np.sign(v)*cf))
    return af, [0]


def RK4Update_with_friction(model,data,q,v,u,dt,cf):
    a1 = aba_with_friction(model,data,q,v,u,cf)
    q1,v1 = pin.integrate(model, q,v*dt/2), v+a1*dt/2
    a2 = aba_with_friction(model,data,q1,v1,u,cf)
    q2,v2 = pin.integrate(model, q,v1*dt/2), v+a2*dt/2
    a3 = aba_with_friction(model,data,q2,v2,u,cf)
    q3,v3 = pin.integrate(model, q,v2*dt), v+a3*dt
    a4 = aba_with_friction(model,data,q3,v3,u,cf)
    return pin.integrate(model, q,(dt/3)*((1/2)*v+v1+v2+(1/2)*v3)), v+ (dt/3)*((1/2)*a1 + a2 + a3 + (1/2)*a4)


def create_pendulum(N):
    #base is fixed
    model = pin.Model()
    geom_model = pin.GeometryModel()

    parent_id = 0
    base_radius = 0.2
    shape_base = fcl.Sphere(base_radius)
    geom_base = pin.GeometryObject("base", 0, shape_base, pin.SE3.Identity())
    geom_base.meshColor = np.array([1.,0.1,0.1,1.])
    geom_model.addGeometryObject(geom_base)
    joint_placement = pin.SE3.Identity()
    body_mass = 1.
    body_radius = 0.1
    for k in range(N):
        joint_name = "joint_" + str(k+1)
        joint_id = model.addJoint(parent_id,pin.JointModelRX(),joint_placement,joint_name)

        body_inertia = pin.Inertia.FromSphere(body_mass,body_radius)
        body_placement = joint_placement.copy()
        body_placement.translation[2] = 1.
        model.appendBodyToJoint(joint_id,body_inertia,body_placement)

        geom1_name = "ball_" + str(k+1)
        shape1 = fcl.Sphere(body_radius)
        geom1_obj = pin.GeometryObject(geom1_name, joint_id, shape1, body_placement)
        geom1_obj.meshColor = np.ones((4))
        geom_model.addGeometryObject(geom1_obj)

        geom2_name = "bar_" + str(k+1)
        shape2 = fcl.Cylinder(body_radius/4.,body_placement.translation[2])
        shape2_placement = body_placement.copy()
        shape2_placement.translation[2] /= 2.

        geom2_obj = pin.GeometryObject(geom2_name, joint_id, shape2, shape2_placement)
        geom2_obj.meshColor = np.array([0.,0.,0.,1.])
        geom_model.addGeometryObject(geom2_obj)

        parent_id = joint_id
        joint_placement = body_placement.copy()
    end_frame=pin.Frame("end_effector_frame",model.getJointId("joint_"+str(N)), 0, body_placement, pin.FrameType(3))
    model.addFrame(end_frame)
    geom_model.collision_pairs = []
    model.qinit = np.zeros(model.nq)
    model.qinit[0] = 1.*np.pi
    model.qref = pin.neutral(model)
    data = model.createData()
    geom_data = geom_model.createData()
    ddl = np.array([0])
    return model, geom_model, data, geom_data, ddl

def create_double_pendulum_erd():
    robot = erd.load("double_pendulum")
    model = robot.model
    end_placement = pin.SE3.Identity()
    end_placement.translation = np.array([0.,0.,0.2])
    end_frame = pin.Frame("end_effector_frame", model.getJointId("joint2"), 0, end_placement, pin.FrameType(3))
    model.addFrame(end_frame)
    model.qinit = np.zeros(model.nq)
    model.qinit[0] = 1.*np.pi
    model.qref = pin.neutral(model)
    data = model.createData()
    geom_model = robot.collision_model
    geom_model.collision_pairs = []
    geom_data = robot.collision_data
    ddl = np.array([0,1])
    return model, geom_model, data, geom_data, ddl

def create_cartpole(N):
    model = pin.Model()
    geom_model = pin.GeometryModel()
    
    parent_id = 0
    
    cart_radius = 0.1
    cart_length = 5 * cart_radius
    cart_mass = 2.
    joint_name = "joint_cart"

    geometry_placement = pin.SE3.Identity()
    geometry_placement.rotation = pin.Quaternion(np.array([0.,0.,1.]),np.array([0.,1.,0.])).toRotationMatrix()

    joint_id = model.addJoint(parent_id, pin.JointModelPY(), pin.SE3.Identity(), joint_name)

    body_inertia = pin.Inertia.FromCylinder(cart_mass,cart_radius,cart_length)
    body_placement = geometry_placement
    model.appendBodyToJoint(joint_id,body_inertia,body_placement) # We need to rotate the inertia as it is expressed in the LOCAL frame of the geometry

    shape_cart = fcl.Cylinder(cart_radius, cart_length)

    geom_cart = pin.GeometryObject("shape_cart", joint_id, shape_cart, geometry_placement)
    geom_cart.meshColor = np.array([1.,0.1,0.1,1.])
    geom_model.addGeometryObject(geom_cart)

    parent_id = joint_id
    joint_placement = pin.SE3.Identity()
    body_mass = 1.
    body_radius = 0.1
    for k in range(N):
        joint_name = "joint_" + str(k+1)
        joint_id = model.addJoint(parent_id,pin.JointModelRX(),joint_placement,joint_name)

        body_inertia = pin.Inertia.FromSphere(body_mass,body_radius)
        body_placement = joint_placement.copy()
        body_placement.translation[2] = 1.
        model.appendBodyToJoint(joint_id,body_inertia,body_placement)

        geom1_name = "ball_" + str(k+1)
        shape1 = fcl.Sphere(body_radius)
        geom1_obj = pin.GeometryObject(geom1_name, joint_id, shape1, body_placement)
        geom1_obj.meshColor = np.ones((4))
        geom_model.addGeometryObject(geom1_obj)

        geom2_name = "bar_" + str(k+1)
        shape2 = fcl.Cylinder(body_radius/4.,body_placement.translation[2])
        shape2_placement = body_placement.copy()
        shape2_placement.translation[2] /= 2.

        geom2_obj = pin.GeometryObject(geom2_name, joint_id, shape2, shape2_placement)
        geom2_obj.meshColor = np.array([0.,0.,0.,1.])
        geom_model.addGeometryObject(geom2_obj)

        parent_id = joint_id
        joint_placement = body_placement.copy()
    end_frame=pin.Frame("end_effector_frame",model.getFrameId(str(parent_id)), parent_id, body_placement, pin.FrameType(2))
    model.addFrame(end_frame)
    geom_model.collision_pairs = []
    model.qinit = np.zeros(model.nq)
    model.qinit[1] = 1.*np.pi
    model.qref = pin.neutral(model)
    data = model.createData()
    geom_data = geom_model.createData()
    ddl = np.array([0])
    return model, geom_model, data, geom_data, ddl

def createSoloModel():
    # robot model
    robot = example_robot_data.load('solo12')
    rmodel = robot.model.copy()
    rdata = rmodel.createData()
    rmodel.qref = rmodel.referenceConfigurations["standing"]
    rmodel.qinit = rmodel.referenceConfigurations["standing"]
    # Geometry model
    rgeomModel = robot.collision_model
    # add feet
    a = 0.01910275#1730829468
    rFoot = np.array([0.2, 0.08, 0.05])
    R = np.eye(3)
    tLeft = np.array([0, 0.008,  -0.16])
    MLeft = pin.SE3(R, tLeft)
    tRight = np.array([0, -0.008,  -0.16])
    MRight = pin.SE3(R, tRight)
    r = np.array([a, a, a])  
    
    
    rgeomModel.computeColPairDist = []

    n = np.array([0., 0., 1])
    p = np.array([0., 0., 0.0])
    h = np.array([100., 100., 0.01])
    plane_shape = Plane(0, 'plane', n, p, h)
    T = pin.SE3(plane_shape.R, plane_shape.t)
    plane = pin.GeometryObject("plane", 0, 0, plane_shape, T)
    plane.meshColor = np.array([0.5, 0.5, 0.5, 1.]) 
    planeId = rgeomModel.addGeometryObject(plane)
    
    frames_names = ["HR_FOOT","HL_FOOT","FR_FOOT","FL_FOOT"]
    
    
    rgeomModel.collision_pairs = []
    for name in frames_names:
        frame_id = rmodel.getFrameId(name)
        frame = rmodel.frames[frame_id]
        joint_id = frame.parent
        frame_placement = frame.placement
        
        shape_name = name + "_shape"
        shape = Ellipsoid(joint_id, shape_name , r, frame_placement)
        geometry = pin.GeometryObject(shape_name, joint_id, shape, frame_placement)
        geometry.meshColor = np.array([1.0, 0.2, 0.2, 0.8])
        
        geom_id = rgeomModel.addGeometryObject(geometry)
        
        foot_plane = CollisionPairPlaneEllipsoid(planeId, geom_id)
        rgeomModel.collision_pairs += [foot_plane]
        rgeomModel.computeColPairDist.append(False)
    rgeom_data = rgeomModel.createData()
    ddl = np.array([i for i in range(6,rmodel.nv)])
    return rmodel, rgeomModel, rdata, rgeom_data, ddl

def createCubeModel():
    rmodel = pin.Model()
    freeflyer = pin.JointModelFreeFlyer()
    jointCube = rmodel.addJoint(0, freeflyer, pin.SE3.Identity(), "joint1")
    M = pin.SE3(np.eye(3), np.matrix([0., 0., 0.]).T)
    rmodel.appendBodyToJoint(jointCube, pin.Inertia.FromBox(1,0.2,0.2,0.2), M)
    rmodel.qref = pin.neutral(rmodel)
    rmodel.qinit = rmodel.qref.copy()
    rmodel.qinit[2] += 0.1
    data = rmodel.createData()
    rgeomModel = pin.GeometryModel()
    a = 0.2
    r = np.array([a/4, a/4, a/4])
    cube_shape = fcl.Box(a, a, a)
    rgeomModel.computeColPairDist = []

    n = np.array([0., 0., 1])
    p = np.array([0., 0., 0.0])
    h = np.array([10., 10., 0.])
    plane_shape = Plane(0, 'plane', n, p, h)
    T = pin.SE3(plane_shape.R, plane_shape.t)
    plane = pin.GeometryObject("plane", 0, 0, plane_shape, T)
    plane.meshColor = np.array([0.5, 0.5, 0.5, 1.]) 
    planeId = rgeomModel.addGeometryObject(plane)

    # add balls to cube
    rgeomModel.collision_pairs = []

    R = pin.utils.eye(3)
    t = np.matrix([a/4, -a/4, -a/4]).T
    ball_shape1 = Ellipsoid(jointCube, 'ball1', r,pin.SE3(R, t)) 
    geom_ball1 = pin.GeometryObject("ball1", jointCube, jointCube, ball_shape1, pin.SE3(R, t)) 
    geom_ball1.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
    ball1Id = rgeomModel.addGeometryObject(geom_ball1)
    col_pair1 = CollisionPairPlaneEllipsoid(planeId, ball1Id)
    col_pair1.computeDistanceResidualBool = True
    rgeomModel.collision_pairs += [col_pair1]

    R = pin.utils.eye(3)
    t = np.matrix([a/4, a/4, -a/4]).T
    ball_shape2 = Ellipsoid(jointCube, 'ball2', r,pin.SE3(R, t)) 
    geom_ball2 = pin.GeometryObject("ball2", jointCube, jointCube, ball_shape2, pin.SE3(R, t)) 
    geom_ball2.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
    ball2Id = rgeomModel.addGeometryObject(geom_ball2)
    col_pair2 = CollisionPairPlaneEllipsoid(planeId, ball2Id)
    col_pair2.computeDistanceResidualBool = True
    rgeomModel.collision_pairs += [col_pair2]

    R = pin.utils.eye(3)
    t = np.matrix([a/4, a/4, a/4]).T
    ball_shape3 = Ellipsoid(jointCube, 'ball3', r,pin.SE3(R, t)) 
    geom_ball3 = pin.GeometryObject("ball3", jointCube, jointCube, ball_shape3, pin.SE3(R, t)) 
    geom_ball3.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
    ball3Id = rgeomModel.addGeometryObject(geom_ball3)
    col_pair3 = CollisionPairPlaneEllipsoid(planeId, ball3Id)
    col_pair3.computeDistanceResidualBool = True
    rgeomModel.collision_pairs += [col_pair3]

    R = pin.utils.eye(3)
    t = np.matrix([a/4, -a/4, a/4]).T
    ball_shape4 = Ellipsoid(jointCube, 'ball4', r,pin.SE3(R, t)) 
    geom_ball4 = pin.GeometryObject("ball4", jointCube, jointCube, ball_shape4, pin.SE3(R, t)) 
    geom_ball4.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
    ball4Id = rgeomModel.addGeometryObject(geom_ball4)
    col_pair4 = CollisionPairPlaneEllipsoid(planeId, ball4Id)
    col_pair4.computeDistanceResidualBool = True
    rgeomModel.collision_pairs += [col_pair4]

    R = pin.utils.eye(3)
    t = np.matrix([-a/4, -a/4, -a/4]).T
    ball_shape5 = Ellipsoid(jointCube, 'ball5', r,pin.SE3(R, t)) 
    geom_ball5 = pin.GeometryObject("ball5", jointCube, jointCube, ball_shape5, pin.SE3(R, t)) 
    geom_ball5.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
    ball5Id = rgeomModel.addGeometryObject(geom_ball5)
    col_pair5 = CollisionPairPlaneEllipsoid(planeId, ball5Id)
    col_pair5.computeDistanceResidualBool = True
    rgeomModel.collision_pairs += [col_pair5]

    R = pin.utils.eye(3)
    t = np.matrix([-a/4, a/4, -a/4]).T
    ball_shape6 = Ellipsoid(jointCube, 'ball6', r,pin.SE3(R, t)) 
    geom_ball6 = pin.GeometryObject("ball6", jointCube, jointCube, ball_shape6, pin.SE3(R, t)) 
    geom_ball6.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
    ball6Id = rgeomModel.addGeometryObject(geom_ball6)
    col_pair6 = CollisionPairPlaneEllipsoid(planeId, ball6Id)
    col_pair6.computeDistanceResidualBool = True
    rgeomModel.collision_pairs += [col_pair6]

    R = pin.utils.eye(3)
    t = np.matrix([-a/4, a/4, a/4]).T
    ball_shape7 = Ellipsoid(jointCube, 'ball7', r,pin.SE3(R, t)) 
    geom_ball7 = pin.GeometryObject("ball7", jointCube, jointCube, ball_shape7, pin.SE3(R, t)) 
    geom_ball7.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
    ball7Id = rgeomModel.addGeometryObject(geom_ball7)
    col_pair7 = CollisionPairPlaneEllipsoid(planeId, ball7Id)
    col_pair7.computeDistanceResidualBool = True
    rgeomModel.collision_pairs += [col_pair7]

    R = pin.utils.eye(3)
    t = np.matrix([-a/4, -a/4, a/4]).T
    ball_shape8 = Ellipsoid(jointCube, 'ball8', r,pin.SE3(R, t)) 
    geom_ball8 = pin.GeometryObject("ball8", jointCube, jointCube, ball_shape8, pin.SE3(R, t)) 
    geom_ball8.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
    ball8Id = rgeomModel.addGeometryObject(geom_ball8)
    col_pair8 = CollisionPairPlaneEllipsoid(planeId, ball8Id)
    col_pair8.computeDistanceResidualBool = True
    rgeomModel.collision_pairs += [col_pair8]

    rgeom_data = rgeomModel.createData()
    ddl = np.array([2])
    return rmodel, rgeomModel, data, rgeom_data, ddl


class SmoothContactActionModel(croc.ActionModelAbstract):
    def __init__(self, state, ddl, simulator, jointCube, ptarget1 , cw1, cw2, noiseIntensity, nbSamples):
        self.sim = simulator
        self.ptarget1 = ptarget1
        self.jointCube = jointCube
        self.costWeight1 = cw1
        self.costWeight2 = cw2
        self.costReg = 5e-7
        self.nv = self.sim.rmodel.nv 
        self.noiseIntensity = noiseIntensity
        self.nbSamples = nbSamples
        self.ddl = ddl
        nu = len(ddl)
        nr = self.nv #+ len(ddl)
        self.unone = np.zeros(nu)
        croc.ActionModelAbstract.__init__(self, state, nu, nr)

    def createData(self):
        data = croc.ActionModelAbstract.createData(self)
        data.node = SimulatorNode(self.sim)
        return data
    
    def calc(self, data, x, u=None, uNoise = None):
        if u is None:
            u = self.unone
        if uNoise is None:
            uNoise = torch.zeros((self.nbSamples, u.shape[0]))
        t_x = torch.from_numpy(x)
        t_xnext = torch.zeros_like(t_x)
        for ui in uNoise:
            t_tau = torch.zeros(self.nv)
            t_tau[self.ddl] = torch.tensor(u) + ui
            t_xnexti = data.node.makeStep(t_x, t_tau, calcPinDiff=False)
            t_xnext += t_xnexti
        t_xnext /= self.nbSamples
        x_tar = torch.zeros_like(t_xnext)
        x_tar[2] = self.ptarget1[2]
        t_res1 = torch.from_numpy(self.costWeight1)*(t_xnext - x_tar)
        data.xnext[:] = t_xnext.detach().numpy()
        t_res_u = torch.from_numpy(self.costWeight2 * u)
        t_res = torch.cat([t_res1, t_res_u])
        data.r = t_res.detach().numpy()
        data.cost =  0.5 * sum(data.r**2)
        return data.xnext, data.cost


    def calcDiff(self, data, x, u=None):
        if u is None:
            u = self.unone
        xnext, cost = self.calc(data, x, u)
        t_x = torch.from_numpy(x).requires_grad_(True)
        t_u = torch.from_numpy(u).requires_grad_(True)
        def calcTorch(t_x, t_u):
            t_xnext = torch.zeros_like(t_x)
            uNoise = self.noiseIntensity*torch.randn((self.nbSamples, t_u.size()[0]))
            for i in range(self.nbSamples): 
                t_tau = torch.zeros(self.nv)
                t_tau[self.ddl] = t_u + uNoise[i]
                t_xnexti = data.node.makeStep(t_x, t_tau, calcPinDiff=True)
                t_xnext += t_xnexti
            t_xnext /= self.nbSamples
            x_tar = torch.zeros_like(t_xnext)
            x_tar[2] = self.ptarget1[2]
            t_res1 = torch.from_numpy(self.costWeight1)*(t_xnext[2] - x_tar[2]).unsqueeze(0)
            t_res_reg =  torch.tensor([0.]) 
            t_res_u = torch.from_numpy(self.costWeight2) * t_u
            t_res = torch.cat([t_res1, t_res_u, t_res_reg])
            t_cost = 0.5 * torch.sum(t_res * t_res)
            return t_xnext, t_cost
        
        J = torch.autograd.functional.jacobian(calcTorch, (t_x, t_u))
        data.Fx[:] = J[0][0].squeeze().numpy()
        data.Fu[:] = J[0][1][:].squeeze().numpy()
        if np.linalg.norm(data.Fu[:])<1e-9:
            data.Fu[:] = 0
        data.Lx[:] = J[1][0].numpy()
        data.Lu[:] = J[1][1].numpy()
        return xnext, cost

class SmoothFrictionActionModel(croc.DifferentialActionModelAbstract):
    def __init__(self, state, ddl, costModel, cf, dt, noiseIntensity, nbSamples, multithreading = False):
        nu = len(ddl)
        croc.DifferentialActionModelAbstract.__init__(self, state, nu, costModel.nr)
        self.costs = costModel
        self.enable_force = True
        self.armature = np.zeros(0)
        self.nbSamples = nbSamples
        self.noiseIntensity = noiseIntensity
        self.costWeights = 0
        self.dt = dt
        self.cf = cf
        self.multithreading = multithreading
        self.ddl = ddl
        self.VarFu = 0

    def calc(self, data, x, u=None, uNoise = None):
        if uNoise is None:
            uNoise = self.noiseIntensity*np.random.normal(size=(self.nbSamples, len(self.ddl)))
        if u is None:
            u = self.unone
        q, v = x[:self.state.nq], x[-self.state.nv:]
        # Computing the dynamics using ABA or manually for armature case
        data.xout[:] = 0
        tau = np.zeros(self.state.pinocchio.nv)
        tau[self.ddl] = u 
        tau_pert = np.zeros((self.nbSamples,self.state.pinocchio.nv))
        tau_pert[:,self.ddl] = tau[self.ddl] +uNoise
        if self.multithreading:
            pool = Pool(self.nbSamples)
            args = [(self.state.pinocchio, data.pinocchio, q, v, U, self.cf) for U in tau]
            results_samples = pool.starmap(aba_with_friction, args)
            data.xout += sum(results_samples)/self.nbSamples
        else:
            for i in range(self.nbSamples):
                a_i, _ = aba_with_friction5(self.state.pinocchio, data.pinocchio, q, v, tau_pert[i], self.cf, self.dt)
                data.xout += a_i/self.nbSamples
        # Computing the cost value and residuals
        pin.forwardKinematics(self.state.pinocchio, data.pinocchio, q, v)
        pin.updateFramePlacements(self.state.pinocchio, data.pinocchio)
        self.costs.calc(data.costs, x, tau)
        data.cost = data.costs.cost
        #print(data.cost)
        


    def calcDiff(self, data, x, u=None):
        q, v = x[:self.state.nq], x[-self.state.nv:]
        if u is None:
            u = self.unone
        uNoise = self.noiseIntensity*np.random.normal(size=(self.nbSamples, len(self.ddl)))
        self.calc(data, x, u, uNoise)
        # Computing the dynamics derivatives
        data.Fx[:] = 0
        data.Fu[:] = 0
        Fus = []
        tau = np.zeros(self.state.pinocchio.nv)
        tau[self.ddl] = u 
        tau_pert = np.zeros((self.nbSamples,self.state.pinocchio.nv))
        tau_pert[:,self.ddl] = tau[self.ddl] +uNoise
        for j in range(self.nbSamples):
            pin.computeABADerivatives(self.state.pinocchio, data.pinocchio, q, v, tau_pert[j])
            Fxi = np.hstack([data.pinocchio.ddq_dq, data.pinocchio.ddq_dv])
            Fui = data.pinocchio.Minv[:,self.ddl]
            for i in range(self.state.nv):
                if np.abs(v)[i]<1e-3:
                    if data.xout[i] != 0:
                        continue
                    else:
                        #Fxi[i][:] = 0
                        Fui[i][:] = 0
                else:
                    continue
            data.Fx[:] += Fxi[0]/self.nbSamples
            if Fui.shape[1] == 1:
                Fui = np.squeeze(Fui, axis = 1)
            data.Fu[:] += Fui[:]/self.nbSamples
            Fus += [Fui]
        # Fus are derivative of acceleration wrt u but we want the derivative of next state  wrt u 
        # requires to differentiate integration step
        self.VarFu = 0#np.cov(np.array(Fus), rowvar=False)
        # Computing the cost derivatives
        self.costs.calcDiff(data.costs, x, tau)

    def createData(self):
        data = croc.DifferentialActionModelAbstract.createData(self)
        data.pinocchio = self.state.pinocchio.createData()#pin.Data(self.state.pinocchio)
        data.multibody = croc.DataCollectorMultibody(data.pinocchio)
        data.costs = self.costs.createData(data.multibody)
        data.costs.shareMemory(data) # this allows us to share the memory of cost-terms of action model#

        return data


def smooth_friction_action_model(state, ddl, Mref, wet_cur, wet_final, wx_cur,wu_reg, wx_final, wx_reg,cf,noiseIntensity,nbSamples, DT):
    efTranslationCost = croc.CostModelFrameTranslation(state, Mref)
    xRegCost = croc.CostModelState(state, croc.ActivationModelWeightedQuad(wx_reg), np.zeros_like(wx_reg))
    u_tar = np.zeros(state.pinocchio.nv) #target control to regularize cost function
    wu_reg_tmp = np.zeros_like(u_tar)
    wu_reg_tmp[ddl] = wu_reg
    uRegCost = croc.CostModelControl(state, croc.ActivationModelWeightedQuad(wu_reg_tmp), u_tar)
    xCost_cur = croc.CostModelState(state, croc.ActivationModelWeightedQuad(wx_cur), np.zeros_like(wx_cur))
    xCost_final = croc.CostModelState(state, croc.ActivationModelWeightedQuad(wx_final), np.zeros_like(wx_final))
    # Create cost model per each action model
    runningCostModel = croc.CostModelSum(state)
    terminalCostModel = croc.CostModelSum(state)
    
    # Then let's add the running and terminal cost functions
    runningCostModel.addCost("endEffectorPose", efTranslationCost, wet_cur[0]/DT)
    runningCostModel.addCost("modelState", xCost_cur, 1e-0/DT)
    runningCostModel.addCost("stateReg", xRegCost, 1e0/DT)
    runningCostModel.addCost("ctrlReg", uRegCost, 1e0/DT)
    terminalCostModel.addCost("endEffectorPose", efTranslationCost, wet_final[0])
    terminalCostModel.addCost("modelState", xCost_final, 1e-0)
    terminalCostModel.addCost("stateReg", xRegCost, 1e0)
    terminalCostModel.addCost("ctrlReg", uRegCost, 1e0)
    pendulumDAM = SmoothFrictionActionModel(state, ddl, runningCostModel, cf, DT, noiseIntensity, nbSamples)
    pendulumDAM.createData()
    terminalPendulumDAM = SmoothFrictionActionModel(state, ddl, terminalCostModel,cf, DT, noiseIntensity, nbSamples)
    terminalPendulumDAM.createData()
    runningModel = croc.IntegratedActionModelEuler(
        pendulumDAM, DT)
    runningModel.nbSamples = pendulumDAM.nbSamples
    runningModel.noiseIntensity = pendulumDAM.noiseIntensity
    terminalModel = croc.IntegratedActionModelEuler(
        terminalPendulumDAM, 0.)
    terminalModel.nbSamples = terminalPendulumDAM.nbSamples
    terminalModel.noiseIntensity = terminalPendulumDAM.noiseIntensity
    return  runningModel, terminalModel


def adaptiveSolve(problem, init_xs, init_us, maxIter = 100, alpha = .9, beta = 1.1, adaptive="None"):
    us = init_us
    xs = init_xs
    T = problem.T
    runningModel = problem.runningModels[0]
    terminalModel = problem.terminalModel
    x0 = problem.x0
    init_Nsamples = runningModel.nbSamples
    init_noise = runningModel.noiseIntensity
    cost = []
    control_values = [init_us]
    for i in range(maxIter):
        #runningModel, terminalModel = cartpole_action_model(state,ddl, wx_cur, wu_reg, wx_final, wx_reg, coeff_friction, noise_intensity, N_samples,DT)
        problem = croc.ShootingProblem(x0, [runningModel] * T, terminalModel)
        ddp = croc.SolverFDDP(problem)
        log = croc.CallbackLogger()

        ddp.setCallbacks([log,
                          croc.CallbackVerbose()])
        # Solving it with the DDP algorithm
        finished = ddp.solve(xs, us, 1)
        xs = ddp.xs
        us = ddp.us.tolist()
        cost += log.costs
        control_values += [us]
        if adaptive=="RM":
            runningModel.noiseIntensity /= (i+1) 
            terminalModel.noiseIntensity /= (i+1) 
        if adaptive=="exp":
            runningModel.noiseIntensity *= alpha 
            runningModel.nbSamples = max(1,int(init_Nsamples*(.99**i)))
            terminalModel.noiseIntensity *= alpha 
            terminalModel.nbSamples = max(1,int(init_Nsamples*(.99**i)))
        if adaptive=="adaptive":
            Vx = ddp.Vx.tolist()
            VarQus = [np.dot(Vx[j],np.dot(runningModel.differential.VarFu,Vx[j].T)) for j,runningModel in enumerate(problem.runningModels)]
            Qus = ddp.Qu.tolist()
            VarQus_inf = np.max([np.max(VarQu) for VarQu in VarQus])
            Qus_inf = np.max([np.max(Qu) for Qu in Qus])
            if VarQus_inf/Qus_inf > 0.5:
                runningModel.noiseIntensity *= alpha 
                terminalModel.noiseIntensity *= alpha
            elif  VarQus_inf/Qus_inf < 0.05:
                runningModel.noiseIntensity *= beta 
                terminalModel.noiseIntensity *= beta
                #runningModel.nbSamples = max(2,int(init_Nsamples*(.99**i)))
                #terminalModel.nbSamples = max(2,int(init_Nsamples*(.99**i)))
        if finished:
            break
    return finished, ddp, cost, control_values