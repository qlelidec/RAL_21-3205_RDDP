#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 09:32:14 2021

@author: qlelidec
"""

from numpy.core.fromnumeric import shape
import pinocchio as pin
import numpy as np
import torch
from torch.autograd.functional import jacobian
import crocoddyl as croc
import example_robot_data
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from diffsim.shapes import Ellipsoid, Plane 
from pinocchio.visualize import MeshcatVisualizer
from diffsim.collision_pairs import CollisionPairPlaneEllipsoid
from diffsim.utils_render import init_viewer_ellipsoids
from diffsim.simulator import Simulator,SimulatorNode
from tqdm import trange, tqdm
from pathlib import Path
import os
from utils.utils import adaptiveSolve, RK4Update_with_friction, aba_with_friction, aba_with_friction5, make_capture, make_randomized_video, make_video, createSoloModel, create_pendulum, create_double_pendulum_erd, create_cartpole, create_acrobot, SmoothFrictionActionModel, SmoothContactActionModel, smooth_friction_action_model, createCubeModel
import argparse
from multiprocessing import Pool, cpu_count



path_curr = Path(os.path.realpath(__file__)).parent

EXP_TYPE="solo"

parser = argparse.ArgumentParser()
parser.add_argument('-et', '--experiment-type', type=str, default=EXP_TYPE)
args = parser.parse_known_args()

EXP_TYPE = args[0].experiment_type

default_params = {"solo":{"lr":.5,"n_iter":20,"DT":1e-3, "DT_SIM":1e-3, "T":100, "coeff_friction": 0.5,"nb_thread":1,"nb_samples":8,"noise_intensity":5e-1,  "wet_current": [0.],"wet_final": [0.], "wu_reg": [1e-4], "wx_reg":[0.], "wx_cur": [0.], "wx_final": [1e0], "q_regularization": 1e-4, "optimizer":"SGD", "adapt_smoothing": "None", "save_path":path_curr/'logs'/'solo_lifting_leg_rsddp_test', "u_path": "None"},
                  "pendulum":{"lr":.5,"n_iter":100,"DT":5e-3, "DT_SIM":1e-3, "T":1000, "coeff_friction": 1e-3,"nb_thread":1,"nb_samples":2,"noise_intensity":1e-2, "wet_current": [0.],"wet_final": [1e2],"wu_reg": [2e-4],"wx_reg":[0., 0.],"wx_cur": [0.,0.], "wx_final": [0.,0.], "q_regularization": 1e-9, "optimizer":"SGD", "adapt_smoothing": "exp", "save_path":path_curr/'logs'/'pendulum_test', "u_path": "None"},
                  "double_pendulum":{"lr":.5,"n_iter":20,"DT":5e-3, "DT_SIM":1e-3, "T":1000, "coeff_friction": [1e-4,1e-4],"nb_thread":1,"nb_samples":4,"noise_intensity":1e-3, "wet_current": [0.],"wet_final": [300.], "wu_reg": [1e-1,1e-1], "wx_reg":[0.,0.,0.,0.], "wx_cur": [0.,0.,0.,0.], "wx_final": [0.,0.,0.,0.], "q_regularization": 1e-9, "optimizer":"SGD", "adapt_smoothing": "None", "save_path":path_curr/'logs'/'double_pendulum_test', "u_path": "None"},
                  "acrobot":{"lr":.5,"n_iter":20,"DT":5e-3, "DT_SIM":1e-3, "T":1000, "coeff_friction": [0.,0.],"nb_thread":1,"nb_samples":3,"noise_intensity":1e-3, "wet_current": [0.],"wet_final": [300.], "wu_reg": [1e-2], "wx_reg":[0.,0.,0.,0.], "wx_cur": [0.,0.,0.,0.], "wx_final": [0.,0.,0.,0.], "q_regularization": 1e-9, "optimizer":"SGD", "adapt_smoothing": "None", "save_path":path_curr/'logs'/'acrobot', "u_path": "None"},
                  "cartpole":{"lr":.5,"n_iter":100,"DT":5e-3, "DT_SIM":1e-3, "T":1500, "coeff_friction": [3*9.81*1e-5,1e-5],"nb_thread":1,"nb_samples":2,"noise_intensity":1e-1, "wet_current": [0.],"wet_final": [1e2], "wu_reg": [1e-3], "wx_reg":[0.,0.,0.,0.], "wx_cur": [0.,0.,0.,0.], "wx_final": [0.,0.,0.,0.], "q_regularization": 1e-9, "optimizer":"SGD", "adapt_smoothing": "None", "save_path":path_curr/'logs'/'cartpole_test', "u_path": "None"},
                  "cube":{"lr":.5,"n_iter":20,"DT":5e-3, "DT_SIM":1e-3, "T":20, "coeff_friction": 0.7,"nb_thread":1,"nb_samples":3,"noise_intensity":2., "wet_current": [0.],"wet_final": [0.], "wu_reg": [1e-5], "wx_reg":[1e-2], "wx_cur": [8e1], "wx_final": [8e1], "q_regularization": 1e-9, "optimizer":"SGD", "adapt_smoothing": "None", "save_path":path_curr/'logs'/'cube_test', "u_path": "None"}}

EXP_ID = 0
LEARNING_RATE = default_params[EXP_TYPE]["lr"]
NUM_ITER = default_params[EXP_TYPE]["n_iter"]
NUM_THREAD = default_params[EXP_TYPE]["nb_thread"]
OPTIMIZER = default_params[EXP_TYPE]["optimizer"]
DT = default_params[EXP_TYPE]["DT"]
DT_SIM = default_params[EXP_TYPE]["DT_SIM"]
TIME_HORIZON = default_params[EXP_TYPE]["T"]
COEFF_FRICTION  = default_params[EXP_TYPE]["coeff_friction"]
NB_SAMPLES = int(default_params[EXP_TYPE]["nb_samples"])
NOISE_INTENSITY = default_params[EXP_TYPE]["noise_intensity"]
ADAPTIVE_SMOOTHING = default_params[EXP_TYPE]["adapt_smoothing"]
WET_CURRENT = default_params[EXP_TYPE]["wet_current"]
WET_FINAL = default_params[EXP_TYPE]["wet_final"]
WU_REGULARIZATION = default_params[EXP_TYPE]["wu_reg"]
WFINAL_STATE = default_params[EXP_TYPE]["wx_final"]
WREG_STATE = default_params[EXP_TYPE]["wx_reg"]
WCURRENT_STATE = default_params[EXP_TYPE]["wx_cur"]
SAVE_PATH = default_params[EXP_TYPE]["save_path"]
U_PATH = default_params[EXP_TYPE]["u_path"]
Q_REGULARIZATION = default_params[EXP_TYPE]["q_regularization"]
EVAL = False
VISUALIZE = False
SAVE_VID = False

parser = argparse.ArgumentParser()
parser.add_argument('-et', '--experiment-type', type=str, default=EXP_TYPE)
parser.add_argument('-eid', '--experiment-id', type=int, default=EXP_ID)
parser.add_argument('-lr', '--learning-rate', type=float, default=LEARNING_RATE)
parser.add_argument('-nbt', '--num-thread', type=int, default=NUM_THREAD)
parser.add_argument('-ni', '--num-iter', type=int, default=NUM_ITER)
parser.add_argument('-opt', '--optimizer', type=str, default=OPTIMIZER)
parser.add_argument('-fri', '--coeff-friction', nargs='+', type=float, default=COEFF_FRICTION)
parser.add_argument('-nbs', '--nb-samples', type=int, default=NB_SAMPLES) 
parser.add_argument('-noi', '--noise-intensity', type=float, default=NOISE_INTENSITY) 
parser.add_argument('-T', '--time-horizon', type=int, default=TIME_HORIZON) 
parser.add_argument('-dt', '--time-step', type=float, default=DT) 
parser.add_argument('-dts', '--time-step-simulation', type=float, default=DT_SIM) 
parser.add_argument('-ada', '--adaptive-smoothing', type=str, default=ADAPTIVE_SMOOTHING)
parser.add_argument('-wetc', '--wet-current', nargs='+', type=float, default=WET_CURRENT)
parser.add_argument('-wetf', '--wet-final', nargs='+', type=float, default=WET_FINAL)
parser.add_argument('-wur', '--wu-regularization', nargs='+', type=float, default=WU_REGULARIZATION)
parser.add_argument('-wxf', '--wx-final', nargs='+', type=float, default=WFINAL_STATE)
parser.add_argument('-wxc', '--wx-current', nargs='+', type=float, default=WCURRENT_STATE)
parser.add_argument('-wxr', '--wx-regularization', nargs='+', type=float, default=WREG_STATE)
parser.add_argument('-qreg', '--q-regularization', type=float, default=Q_REGULARIZATION) 
parser.add_argument('-svp', '--save-path', type=str, default=SAVE_PATH) 
parser.add_argument('-upth', '--u-path', type=str, default=U_PATH) 
parser.add_argument('-vis', '--visualize', type=bool, default=VISUALIZE) 
parser.add_argument('-svv', '--save-vid', type=bool, default=SAVE_VID) 
parser.add_argument('-eva', '--eval-opt', type=bool, default=EVAL) 

#get and save command line arguments
args = parser.parse_args()
args_as_list = args._get_kwargs()
os.makedirs(args.save_path/str(args.experiment_id), exist_ok=True)
param_file = open(args.save_path/str(args.experiment_id)/"params.txt", 'w')
for item in args_as_list:
    param_file.write("%s\n" % str(item))
param_file.close()


#build the pinocchio model of physical system
if args.experiment_type =="solo":
    rmodel, rgeomModel, rdata, rgeom_data, ddl = createSoloModel()
elif args.experiment_type =="pendulum":
    rmodel, rgeomModel, rdata, rgeom_data, ddl = create_pendulum(1)
elif args.experiment_type =="double_pendulum":
    rmodel, rgeomModel, rdata, rgeom_data, ddl = create_double_pendulum_erd()
elif args.experiment_type =="acrobot":
    rmodel, rgeomModel, rdata, rgeom_data, ddl = create_acrobot()
elif args.experiment_type =="cartpole":
    rmodel, rgeomModel, rdata, rgeom_data, ddl = create_cartpole(1)
elif args.experiment_type =="cube":
    rmodel, rgeomModel, rdata, rgeom_data, ddl = createCubeModel()

#define reference and target positions
q_init = rmodel.qinit

v_init = np.zeros(rmodel.nv)
pin.forwardKinematics(rmodel, rdata, q_init)
pin.updateGeometryPlacements(rmodel, rdata, rgeomModel, rgeom_data)

q0 = q_init.copy()
q_target = q_init.copy()
if args.experiment_type =="solo":
    q_target[-1] += .9
elif args.experiment_type =="pendulum":
    q_target = np.zeros(rmodel.nq)
    target = np.array([0., 0., 1.])
    Mref = croc.FrameTranslation(rmodel.getFrameId("end_effector_frame"), target)
elif args.experiment_type =="double_pendulum" or args.experiment_type =="acrobot":
    q_target = np.zeros(rmodel.nq)
    target = np.array([0.0290872, 0., 0.335])
    Mref = croc.FrameTranslation(rmodel.getFrameId("end_effector_frame"), target)
elif args.experiment_type =="cartpole":
    q_target = np.zeros(rmodel.nq)
    target = np.array([0., 0., 1.])
    Mref = croc.FrameTranslation(rmodel.getFrameId("end_effector_frame"), target)
elif args.experiment_type =="cube":
    q_target[2] += 0.1
dq0 = pin.difference(rmodel, rmodel.qref, q0)
v0 = np.zeros(rmodel.nv)
dqtarget = pin.difference(rmodel, rmodel.qref, q_target)
vtarget = v0.copy()

#get the problem parameters from command input
coeff_friction = args.coeff_friction
coeff_rest = 0.0
dt = args.time_step
T = args.time_horizon
N_iter = int(args.num_iter)
N_thread = args.num_thread
adaptive_smoothing = True
theta_ad = .001
N_samples, noise_intensity = args.nb_samples, args.noise_intensity
alpha = args.learning_rate
wet_cur = np.array(args.wet_current)
wet_final = np.array(args.wet_final)
wu_reg = np.array(args.wu_regularization)
wx_reg = np.array(args.wx_regularization)
wx_final = np.array(args.wx_final)
wx_cur = np.array(args.wx_current)
qreg= args.q_regularization

if args.experiment_type == "solo" or args.experiment_type == "cube":
    state = croc.StateVector(2*rmodel.nv)
    x0 = np.zeros(2*rmodel.nv)
    x0[:rmodel.nv] = dq0
    x0[rmodel.nv:] = v0
else:
    state =croc.StateMultibody(rmodel) #croc.StateVector(2*rmodel.nv)
    x0 = np.zeros(rmodel.nv+rmodel.nq)
    x0[:rmodel.nq] = q0
    x0[rmodel.nq:] = v0

#initialize control variables 
if args.experiment_type=="solo":
    #u_init = np.load(path_curr/"utils"/"solo12_quasistatic_u.npy")
    u_init = np.load(path_curr/"utils"/"solo12_quasistatic_ucorr1e-3.npy")[:T]
    u_target = u_init.copy()
    #u_target = np.zeros_like(u_init)
elif args.experiment_type=="cube":
    u_init= np.zeros(len(ddl)) +7.
    u_target = np.zeros_like(u_init)
else:
    u_init= np.zeros(len(ddl)) #-0.01 #0 leads to numerical singularity for pendulum
    u_target = np.zeros_like(u_init)



jointBaseId = 1
#create cost and dynamics functions (action-model)
sim = Simulator(rmodel, rgeomModel, dt, coeff_friction, coeff_rest, dt_collision=dt, steps_contact_cascade=15)
print("CPU count = %d" % cpu_count())
if N_thread > 1:
    pool = Pool(min(N_thread, cpu_count()))
    print("Number of thread: ", len(pool._pool))
else:
    pool = None
    print("Number of thread: ", 1)

if args.experiment_type =="cube":
    adaptive = True if (args.adaptive_smoothing=="adaptive" or args.adaptive_smoothing=="cascade") else False
    actionModel1 = SmoothContactActionModel(state, ddl, sim, jointBaseId, dqtarget, u_target, wx_cur, wu_reg, noise_intensity, N_samples, adaptive)
    actionModelTerminal = SmoothContactActionModel(state, ddl, sim, jointBaseId, dqtarget, u_target, wx_final, wu_reg, noise_intensity, N_samples, adaptive)
    if args.eval_opt:
        actionModel_eval = SmoothContactActionModel(state, ddl, sim, jointBaseId, dqtarget, u_target, wx_cur, wu_reg, 0., 1, None)
        actionModelTerminal_eval = SmoothContactActionModel(state, ddl, sim, jointBaseId, dqtarget, u_target, wx_final, wu_reg, 0., 1, None)
    pool = None
elif args.experiment_type =="solo":
    adaptive = True if (args.adaptive_smoothing=="adaptive" or args.adaptive_smoothing=="cascade") else False
    actionModel1 = [SmoothContactActionModel(state, ddl, sim, jointBaseId, dqtarget, u_target[i], wx_cur, wu_reg, noise_intensity, N_samples, adaptive) for i in range(T)]
    actionModelTerminal = SmoothContactActionModel(state, ddl, sim, jointBaseId, dqtarget, u_target[-1], wx_final, wu_reg, noise_intensity, N_samples, adaptive)
    if args.eval_opt:
        actionModel_eval = [SmoothContactActionModel(state, ddl, sim, jointBaseId, dqtarget, u_target[i], wx_cur, wu_reg, 0., 1, None) for i in range(T)]
        actionModelTerminal_eval = SmoothContactActionModel(state, ddl, sim, jointBaseId, dqtarget, u_target[-1], wx_final, wu_reg, 0., 1, None)
    pool = None
else:
    actionModel1, actionModelTerminal, pool = smooth_friction_action_model(state, ddl, Mref, u_target, wet_cur, wet_final, wu_reg, wx_cur, wx_final, wx_reg, coeff_friction, noise_intensity, N_samples, pool ,dt)
    actionModel_eval, actionModelTerminal_eval, pool = smooth_friction_action_model(state, ddl, Mref, u_target, wet_cur, wet_final, wu_reg, wx_cur, wx_final, wx_reg, coeff_friction, 0., 1, pool ,dt)



#set the optimal control problem
if args.experiment_type=="solo":
    problem = croc.ShootingProblem(x0, actionModel1, actionModelTerminal)
    init_us = [u_init[i] for i in range(len(u_init))]
else:
    problem = croc.ShootingProblem(x0, [actionModel1]*T, actionModelTerminal)
    init_us = [u_init] * T

if args.eval_opt:
    if args.experiment_type=="solo":
        eval_problem = croc.ShootingProblem(x0, actionModel_eval, actionModelTerminal_eval)
    else:
        eval_problem = croc.ShootingProblem(x0, [actionModel_eval]*T, actionModelTerminal_eval)
else:
    eval_problem = None

if args.u_path != "None":
    u_init = torch.load(args.u_path)
    init_us = [u_init[i].detach().numpy() for i  in range(T)]
init_xs = problem.rollout(init_us)
cost_init = problem.calc(init_xs,init_us)
#solve optimal control problem with DDP
done, ddp, cost, cost_eval, control_values, grads, grad_vars, epsilons = adaptiveSolve(problem,init_xs, init_us, N_iter, qreg= qreg, experiment_type=args.experiment_type, adaptive = args.adaptive_smoothing, eval_problem=eval_problem)
if pool is not None:
    pool.close()
cost = [cost_init]+cost
if args.eval_opt:
    xs_eval = eval_problem.rollout(init_us)
    cost_eval = [eval_problem.calc(xs_eval,init_us)]+cost_eval
#save optimal control variable
u = torch.tensor(np.array(control_values[-1]))
#u = torch.tensor(np.array(ddp.us.tolist()))
torch.save(u,args.save_path/str(args.experiment_id)/'u.pt')

#generate the trajectory resulting from the optimal control
#in order to plot the results
dt_sim = args.time_step_simulation
T_sim = T*int(dt/dt_sim)
sim_nodes_demo = [SimulatorNode( Simulator(rmodel, rgeomModel, dt_sim, coeff_friction, coeff_rest, steps_contact_cascade = 30, dt_collision=dt_sim, k_baumgarte=.1, eps_contact=1e-6))]*T_sim
traj = np.zeros((T_sim+1,rmodel.nq))
traj_pert = np.zeros((N_samples,T_sim+1,rmodel.nq))
contact_forces = []
xnext = torch.cat((torch.tensor(dq0),torch.tensor(v0)), dim=0).detach().clone()
qi,vi = q_init, v_init
pattes_positions = [np.array([rgeom_data.oMg[-1].translation[2], rgeom_data.oMg[-2].translation[2], rgeom_data.oMg[-3].translation[2], rgeom_data.oMg[-4].translation[2]])] if args.experiment_type =="solo" else []
acc_com = []
masstot = 0
for bodymass in rmodel.inertias.tolist():
    masstot += bodymass.mass
traj[0,:] = q_init
traj_pert[:,0,:]= q_init
u_sim = [u[i//int(dt/dt_sim)] for i  in range(T_sim)]
for t in range(T_sim):
    t_tau = torch.zeros(rmodel.nv)
    t_tau[ddl] = u_sim[t]
    if args.experiment_type =="solo" or args.experiment_type =="cube":
        for l in range(N_samples):
            noisy_tau = torch.zeros(rmodel.nv)
            noisy_tau[ddl] = u_sim[t] + 1000*noise_intensity*torch.normal(0.,1.,size=u_sim[t].shape) # we increase the noise to make it visible in the video
            xnext_l = sim_nodes_demo[t].makeStep(xnext, noisy_tau, calcPinDiff=False)  
            traj_pert[l,t+1,:] = pin.integrate(rmodel, rmodel.qref,xnext_l[:rmodel.nv].detach().numpy())
        xnext = sim_nodes_demo[t].makeStep(xnext, t_tau, calcPinDiff=False)
        pin.updateGeometryPlacements(rmodel, sim_nodes_demo[t].rdata, rgeomModel, rgeom_data)
        traj[t+1,:] = pin.integrate(rmodel, rmodel.qref,xnext[:rmodel.nv].detach().numpy())
        contact_forces += [sim_nodes_demo[t].rdata.contact_forces]
        acc_com += [sum(sim_nodes_demo[t].rdata.contact_forces[:,2])/(masstot*dt_sim) - 9.81]
        pattes_positions += [np.array([rgeom_data.oMg[-1].translation[2], rgeom_data.oMg[-2].translation[2], rgeom_data.oMg[-3].translation[2], rgeom_data.oMg[-4].translation[2]])]
    else:
        ai,_ = aba_with_friction5(rmodel, rdata, qi, vi, t_tau.detach().numpy(), coeff_friction*np.ones(rmodel.nv),dt_sim)
        for l in range(N_samples):
            noisy_tau = torch.zeros(rmodel.nv)
            noisy_tau[ddl] = u_sim[t] + 1000*noise_intensity*torch.normal(0.,1.,size=u_sim[t].shape) # we increase the noise to make it visible in the video
            al,_ = aba_with_friction5(rmodel, rdata, qi, vi, noisy_tau.detach().numpy(), coeff_friction*np.ones(rmodel.nv),dt_sim)
            vl = vi + al*dt_sim
            ql = pin.integrate(rmodel,qi, dt_sim*vl)        
            traj_pert[l,t+1,:] = ql
        vi = vi + ai*dt_sim
        qi = pin.integrate(rmodel,qi, dt_sim*vi)        
        traj[t+1,:] = qi

#plot results obtained with the optimal control
plt.rcParams.update({'font.size': 14, 'lines.linewidth':3.})
fig = plt.figure()
ax = fig.gca()
plt.plot([i for i in range(len(cost))], cost)
plt.xlabel("Iterations", fontsize=18)
plt.ylabel("Cost", fontsize=18)
plt.grid(linestyle ="--", linewidth = .4)
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
plt.tight_layout()
plt.savefig(args.save_path/str(args.experiment_id)/'cost_optim.pdf')
plt.close()

fig = plt.figure()
ax = fig.gca()
plt.plot([i for i in range(len(cost_eval))], cost_eval)
plt.xlabel("Iterations", fontsize=18)
plt.ylabel("Cost", fontsize=18)
plt.grid(linestyle ="--", linewidth = .4)
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
plt.tight_layout()
plt.savefig(args.save_path/str(args.experiment_id)/'cost_eval.pdf')
plt.close()

fig = plt.figure()
ax = fig.gca()
plt.plot([i for i in range(len(grads))], grads)
plt.xlabel("Iterations", fontsize=18)
plt.ylabel(r'$|| Q_u||_\infty$', fontsize=18)
plt.grid(linestyle ="--", linewidth = .4)
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
plt.tight_layout()
plt.savefig(args.save_path/str(args.experiment_id)/'grad_optim.pdf')
plt.close()

fig = plt.figure()
ax = fig.gca()
plt.plot([i for i in range(len(grad_vars))], grad_vars)
plt.xlabel("Iterations", fontsize=18)
plt.ylabel(r'$||\mathrm{Var}\ Q_u ||_\infty$', fontsize=18)
plt.grid(linestyle ="--", linewidth = .4)
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
plt.tight_layout()
plt.savefig(args.save_path/str(args.experiment_id)/'grad_var_optim.pdf')
plt.close()

fig = plt.figure()
ax = fig.gca()
plt.plot([i for i in range(len(grad_vars))], [grad_vars[i]/grads[i] for i in range(len(grad_vars))])
plt.xlabel("Iterations", fontsize=18)
plt.ylabel(r'$\frac{||\mathrm{Var}\ Q_u ||_\infty}{|| Q_u||_\infty^2}$', fontsize=18)
plt.grid(linestyle ="--", linewidth = .4)
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
plt.tight_layout()
plt.savefig(args.save_path/str(args.experiment_id)/'signal_noise_ratio_optim.pdf')
plt.close()

fig = plt.figure()
ax = fig.gca()
plt.plot([i for i in range(len(epsilons))], epsilons)
plt.xlabel("Iterations", fontsize=18)
plt.ylabel(r'$\epsilon$', fontsize=18)
plt.grid(linestyle ="--", linewidth = .4)
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
plt.tight_layout()
plt.savefig(args.save_path/str(args.experiment_id)/'epsilons.pdf')
plt.close()

fig = plt.figure()
ax = fig.gca()
for j in range(len(control_values)):
    plt.plot([i*dt for i in range(len(control_values[0]))], control_values[j], alpha = .97**(N_iter-j), color = '#348ABD')
plt.xlabel("Time (s)")
plt.ylabel("Control torque")
plt.grid(linestyle ="--", linewidth = .4)
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
plt.tight_layout()
plt.savefig(args.save_path/str(args.experiment_id)/'control_optim.pdf')
plt.close()

np.save(args.save_path/str(args.experiment_id)/'cost.npy',cost)
np.save(args.save_path/str(args.experiment_id)/'cost_eval.npy',cost_eval)
np.save(args.save_path/str(args.experiment_id)/'control.npy', control_values)
np.save(args.save_path/str(args.experiment_id)/'grad.npy', grads)

if args.experiment_type =="solo":
    fig = plt.figure()
    ax = fig.gca()
    plt.plot([i*dt_sim for i in range(traj.shape[0])], traj[:,2], color = '#348ABD')
    plt.xlabel("Time")
    plt.ylabel("Altitude")
    plt.grid(linestyle ="--", linewidth = .4)
    plt.tight_layout()
    plt.close()

    fig = plt.figure()
    ax = fig.gca()
    for k in range(contact_forces[0].shape[0]):
        plt.plot([i*dt_sim for i in range(len(contact_forces))], [contact_force[k,2] for contact_force in contact_forces], label="contact "+ str(k))
    plt.xlabel("Time (s)")
    plt.ylabel("contact forces")
    plt.grid(linestyle ="--", linewidth = .4)
    plt.legend()
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    plt.tight_layout()
    plt.savefig(args.save_path/str(args.experiment_id)/'contact_forces_values.pdf')
    plt.close()

    fig = plt.figure()
    ax = fig.gca()
    for k in range(pattes_positions[0].shape[0]):
        plt.plot([i*dt_sim for i in range(len(pattes_positions))], [patte_position[k] for patte_position in pattes_positions])
    plt.xlabel("Time (s)")
    plt.ylabel("Positions of feet")
    plt.grid(linestyle ="--", linewidth = .4)
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    plt.tight_layout()
    plt.savefig(args.save_path/str(args.experiment_id)/'feet_values.pdf')
    plt.close()
    np.save(args.save_path/str(args.experiment_id)/'feet_positions.npy', pattes_positions)

    fig = plt.figure()
    ax = fig.gca()
    plt.plot([i*dt_sim for i in range(len(acc_com))], acc_com, color = '#348ABD')
    plt.xlabel("Time (s))")
    plt.ylabel("Acc CoM (m/s^2)")
    plt.grid(linestyle ="--", linewidth = .4)
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    plt.tight_layout()
    plt.savefig(args.save_path/str(args.experiment_id)/'CoMacc_values.pdf')
    plt.close()

if args.experiment_type =="cube":
    plt.figure()
    plt.plot([i*dt_sim for i in range(traj.shape[0])], traj[:,2], color = '#348ABD')
    plt.xlabel("Time (s)")
    plt.ylabel("Altitude")
    plt.grid(linestyle ="--", linewidth = .4)
    plt.tight_layout()
    plt.savefig(args.save_path/str(args.experiment_id)/'altitude.pdf')
    plt.close()
    np.save(args.save_path/str(args.experiment_id)/'traj.npy', traj)

    plt.figure()
    for k in range(contact_forces[0].shape[0]):
        plt.plot([i*dt_sim for i in range(len(contact_forces))], [contact_force[k,2] for contact_force in contact_forces],label="contact "+ str(k))
    plt.xlabel("Time (s")
    plt.ylabel("contact forces")
    plt.grid(linestyle ="--", linewidth = .4)
    plt.tight_layout()
    plt.legend()
    plt.savefig(args.save_path/str(args.experiment_id)/'contact_forces_values.pdf')
    plt.close()


elif args.experiment_type =="pendulum":
    plt.figure()
    plt.plot([i*dt_sim for i in range(traj.shape[0])], traj[:,0], color = '#348ABD')
    plt.xlabel("Time (s)")
    plt.ylabel("Angle")
    plt.grid(linestyle ="--", linewidth = .4)
    plt.tight_layout()
    plt.savefig(args.save_path/str(args.experiment_id)/"angle_values.pdf") 
    plt.close()

elif args.experiment_type =="cartpole":
    plt.figure()
    plt.plot([i*dt_sim for i in range(traj.shape[0])], traj[:,1], color = '#348ABD')
    plt.xlabel("Time (s)")
    plt.ylabel("Angle")
    plt.grid(linestyle ="--", linewidth = .4)
    plt.tight_layout()
    plt.savefig(args.save_path/str(args.experiment_id)/"angle_values.pdf")
    plt.figure()
    plt.plot([i*dt_sim for i in range(traj.shape[0])], traj[:,0], color = '#348ABD')
    plt.xlabel("Time (s)")
    plt.ylabel("Translation")
    plt.grid(linestyle ="--", linewidth = .4)
    plt.tight_layout()
    plt.savefig(args.save_path/str(args.experiment_id)/"translation_values.pdf")
    plt.close()

elif args.experiment_type =="double_pendulum" or args.experiment_type =="acrobot":
    plt.figure()
    plt.plot([i*dt_sim for i in range(traj.shape[0])], traj[:,0], color = '#348ABD')
    plt.xlabel("Time (s)")
    plt.ylabel("Angle")
    plt.grid(linestyle ="--", linewidth = .4)
    plt.tight_layout()
    plt.savefig(args.save_path/str(args.experiment_id)/"angle_values0.pdf")
    plt.figure()
    plt.plot([i*dt_sim for i in range(traj.shape[0])], traj[:,1], color = '#348ABD')
    plt.xlabel("Time (s)")
    plt.ylabel("Angle")
    plt.grid(linestyle ="--", linewidth = .4)
    plt.tight_layout()
    plt.savefig(args.save_path/str(args.experiment_id)/"angle_values1.pdf")
    plt.close()

#visualize the trajectory from the optimal control
if args.visualize and (args.experiment_type == "solo" or args.experiment_type == "cube"):    
    name = "solo_lift_leg"
    viz_lift = init_viewer_ellipsoids(rmodel, rgeomModel, open=True)
    viz_lift.display(q_init)
    print("set camera angle in the viewer and press enter when ready")
    input()
    fps = min(int(1./dt_sim),20)
    max_frames = 100
    if T_sim>max_frames:
        sample_T = int(traj.shape[0]/max_frames )   
        traj_new = np.array([traj[i*sample_T] for i in range(traj.shape[0]//sample_T)]+[traj[-1]])
        fps = min(int(max_frames/(dt_sim*T_sim)),20)
        traj = traj_new
    os.makedirs(args.save_path/str(args.experiment_id)/"captures", exist_ok=True)
    make_capture(viz_lift,q_init,args.save_path/str(args.experiment_id)/"captures"/'init_pose.png')
    make_capture(viz_lift,q_target,args.save_path/str(args.experiment_id)/"captures"/'target_pose_pose.png')
    capture = True
    if capture:
            positions = np.concatenate((np.expand_dims(q_init,axis=0),traj), axis = 0)
            for i,q_i in enumerate(tqdm(positions)):
                if i%(len(positions)//10)==0 or i==len(positions)-1 :
                    make_capture(viz_lift, q_i, args.save_path/str(args.experiment_id)/"captures"/('opt_pose_'+str(i)+'.png'))
    os.makedirs(args.save_path/str(args.experiment_id)/"videos", exist_ok=True)
    make_video(viz_lift, np.concatenate((np.expand_dims(q_init,axis=0),traj), axis = 0), args.save_path/str(args.experiment_id)/"videos"/'adaptiveDDP.mp4', fps, args.save_vid)
elif args.visualize and args.experiment_type != "solo":
    name = "viz_final_traj"
    viz = MeshcatVisualizer(rmodel, rgeomModel, rgeomModel)
    viz.initViewer(open=True)
    viz.loadViewerModel(name)
    viz. display(q_init)
    print("set camera angle in the viewer and press enter when ready")
    input()
    fps = int(1./dt_sim)
    max_frames = 100
    if T_sim>max_frames:
        sample_T = int(traj.shape[0]/max_frames )   
        traj_new = np.array([traj[i*sample_T] for i in range(traj.shape[0]//sample_T)]+[traj[-1]])
        traj_pert_new = np.repeat(traj_new[np.newaxis,:, :], N_samples, axis=0)
        for j in range(N_samples):
            traj_pert_new[j] = np.array([traj_pert[j,i*sample_T] for i in range(traj_pert.shape[1]//sample_T)]+[traj_pert[j,-1]])
        fps = int(max_frames/(dt_sim*T_sim))
        traj = traj_new
    os.makedirs(args.save_path/str(args.experiment_id)/"captures", exist_ok=True)
    make_capture(viz,q_init,args.save_path/str(args.experiment_id)/"captures"/'init_pose.png')
    make_capture(viz,q_target,args.save_path/str(args.experiment_id)/"captures"/'target_pose.png')
    capture = True
    if capture:
            positions = np.concatenate((np.expand_dims(q_init,axis=0),traj), axis = 0)
            for i,q_i in enumerate(tqdm(positions)):
                if i%(len(positions)//10)==0 or i==len(positions)-1 :
                    make_capture(viz, q_i, args.save_path/str(args.experiment_id)/"captures"/('opt_pose_'+str(i)+'.png'))
    os.makedirs(args.save_path/str(args.experiment_id)/"videos", exist_ok=True)
    make_video(viz, np.concatenate((np.expand_dims(q_init,axis=0),traj), axis = 0), args.save_path/str(args.experiment_id)/"videos"/'adaptiveDDP.mp4', fps, args.save_vid)
    make_randomized_video(viz,rmodel, rgeomModel, traj, traj_pert, args.save_path/str(args.experiment_id)/"videos"/'randomized_traj.mp4', fps, args.save_vid)
    
