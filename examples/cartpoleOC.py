#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 30 10:48:40 2021

@author: qlelidec
"""

import pinocchio as pin
import numpy as np
import hppfcl as fcl
import crocoddyl as cdl
import matplotlib.pyplot as plt

def create_pendulum(N):
    #base is fixed
    model = pin.Model()
    geom_model = pin.GeometryModel()

    parent_id = 0
    base_radius = 0.2
    shape_base = fcl.Sphere(base_radius)
    geom_base = pin.GeometryObject("base", 0, shape_base, pin.SE3.Identity())
    geom_base.meshColor = np.array([1.,0.1,0.1,1.])
    geom_model.addGeometryObject(geom_base)
    joint_placement = pin.SE3.Identity()
    body_mass = 1.
    body_radius = 0.1
    for k in range(N):
        joint_name = "joint_" + str(k+1)
        joint_id = model.addJoint(parent_id,pin.JointModelRX(),joint_placement,joint_name)

        body_inertia = pin.Inertia.FromSphere(body_mass,body_radius)
        body_placement = joint_placement.copy()
        body_placement.translation[2] = 1.
        model.appendBodyToJoint(joint_id,body_inertia,body_placement)

        geom1_name = "ball_" + str(k+1)
        shape1 = fcl.Sphere(body_radius)
        geom1_obj = pin.GeometryObject(geom1_name, joint_id, shape1, body_placement)
        geom1_obj.meshColor = np.ones((4))
        geom_model.addGeometryObject(geom1_obj)

        geom2_name = "bar_" + str(k+1)
        shape2 = fcl.Cylinder(body_radius/4.,body_placement.translation[2])
        shape2_placement = body_placement.copy()
        shape2_placement.translation[2] /= 2.

        geom2_obj = pin.GeometryObject(geom2_name, joint_id, shape2, shape2_placement)
        geom2_obj.meshColor = np.array([0.,0.,0.,1.])
        geom_model.addGeometryObject(geom2_obj)

        parent_id = joint_id
        joint_placement = body_placement.copy()
    end_frame=pin.Frame("end_pendulum",model.getJointId("joint_"+str(N)), 0, body_placement, pin.FrameType(3))
    model.addFrame(end_frame)
    return model, geom_model


class PendulumFwdDynamics(cdl.DifferentialActionModelAbstract):
    def __init__(self, state, costModel):
        cdl.DifferentialActionModelAbstract.__init__(self, state, state.nv, costModel.nr)
        self.costs = costModel
        self.enable_force = True
        self.armature = np.zeros(0)

    def calc(self, data, x, u=None):
        #print(x)
        if u is None:
            u = self.unone
        q, v = x[:self.state.nq], x[-self.state.nv:]
        # Computing the dynamics using ABA or manually for armature case
        data.xout += pin.aba(self.state.pinocchio, data.pinocchio, q, v, u)
        #print("u", u)
        #print(data.xout)
        # Computing the cost value and residuals
        pin.forwardKinematics(self.state.pinocchio, data.pinocchio, q, v)
        pin.updateFramePlacements(self.state.pinocchio, data.pinocchio)
        self.costs.calc(data.costs, x, u)
        data.cost = data.costs.cost
        #print(data.cost)

    def calcDiff(self, data, x, u=None):
        q, v = x[:self.state.nq], x[-self.state.nv:]
        if u is None:
            u = self.unone
        self.calc(data, x, u)
        # Computing the dynamics derivatives
        pin.computeABADerivatives(self.state.pinocchio, data.pinocchio, q, v, u)
        
        Fxi = np.hstack([data.pinocchio.ddq_dq, data.pinocchio.ddq_dv])
        Fui = data.pinocchio.Minv
        
        data.Fx += Fxi[0]
        data.Fu += Fui
        #print("2",data.Fu,data.Fx)
        self.costs.calcDiff(data.costs, x, u)
        

    def createData(self):
        data = cdl.DifferentialActionModelAbstract.createData(self)
        data.pinocchio = self.state.pinocchio.createData()#pin.Data(self.state.pinocchio)
        data.multibody = cdl.DataCollectorMultibody(data.pinocchio)
        data.costs = self.costs.createData(data.multibody)
        data.costs.shareMemory(data) # this allows us to share the memory of cost-terms of action model#

        return data
    
model, geom_model = create_pendulum(1)
data = model.createData()
geom_data = geom_model.createData()
q_init = np.zeros(model.nq) + 1.*np.pi
v_init = np.zeros(model.nv)
a_init = np.zeros(model.nv)
x0 = np.concatenate([q_init, v_init])
DT = 5e-3

target = np.array([0., 0., 1.])
Mref = cdl.FrameTranslation(model.getFrameId("end_pendulum"), target)
state = cdl.StateMultibody(model)
goalTrackingCost = cdl.CostModelFrameTranslation(state, Mref)
uRegCost = cdl.CostModelControl(state)

w_stateCost = np.array([0.,1e-1])
xCost = cdl.CostModelState(state, cdl.ActivationModelWeightedQuad(w_stateCost), np.zeros(model.nq + model.nv))

runningCostModel = cdl.CostModelSum(state)
terminalCostModel = cdl.CostModelSum(state)

runningCostModel.addCost("pendulumPose", goalTrackingCost, 2e-0/DT)
runningCostModel.addCost("pendulumPose2", xCost, 2e-1/DT)
runningCostModel.addCost("ctrlReg", uRegCost, 2e-3/DT)
terminalCostModel.addCost("pendulumPose", goalTrackingCost, 2e-0)
terminalCostModel.addCost("pendulumPose2", xCost, 2e-1)
terminalCostModel.addCost("ctrlReg", uRegCost, 2e-3)

pendulumDAM = PendulumFwdDynamics(state, runningCostModel)
pendulumDAM.createData()
terminalPendulumDAM = PendulumFwdDynamics(state, terminalCostModel)
terminalPendulumDAM.createData()
runningModel = cdl.IntegratedActionModelRK4(
    pendulumDAM, DT)
terminalModel = cdl.IntegratedActionModelRK4(
    terminalPendulumDAM, 0.)

T = 400
problem = cdl.ShootingProblem(x0, [runningModel] * T, terminalModel)

ddp = cdl.SolverFDDP(problem)
log = cdl.CallbackLogger()
ddp.setCallbacks([log, cdl.CallbackVerbose()])

init_us = [np.zeros_like(a_init)]*T
init_xs = problem.rollout(init_us)
ddp.th_stop = 1e-12
done = ddp.solve(init_xs,init_us,40)

print(done)
print(list(ddp.xs)[-1])
# cdl.plotOCSolution(log.xs, log.us, figIndex=1, show=False)
# cdl.plotConvergence(log.costs, log.u_regs, log.x_regs, log.grads, log.stops, log.steps, figIndex=2)

# plt.figure()
# plt.plot([i*DT for i in range(len(ddp.xs))], np.array(list(ddp.xs))[:,0], label = "angle")
# plt.legend()
# plt.show()