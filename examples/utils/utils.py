#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  4 11:02:14 2021

@author: qlelidec
"""

import pinocchio as pin
from pinocchio.visualize import MeshcatVisualizer
import numpy as np
from tqdm import trange
import imageio
import os
from pathlib import Path
import copy
from tqdm import tqdm
import time
import torch
from qcqp import BoxQPFn2, SignedBoxQPFn2
import hppfcl as fcl 
import example_robot_data
from diffsim.shapes import Ellipsoid, Plane 
from diffsim.collision_pairs import CollisionPairPlaneEllipsoid
import crocoddyl as croc
from diffsim.simulator import SimulatorNode
import example_robot_data as erd

def make_video(viz, positions, name, fps, save):
    imgs = []
    print("visualizing trajectory")
    for i,q_i in enumerate(tqdm(positions)):
        viz.display(q_i)
        time.sleep(1./fps)
    if save:
        print("generating frames")
        for q_i in tqdm(positions):
            viz.display(q_i)
            img_i = np.array(viz.viewer.get_image())
            imgs += [img_i]
        
        writer = imageio.get_writer(name, fps=fps)
        for im in imgs:
            writer.append_data(im)
        writer.close()
    return 

def make_randomized_video(viz,rmodel,rgeomModel,positions,positions_pert,name,fps,save):
    imgs = []
    NbSamples = positions_pert.shape[0]
    print("visualizing randomized trajectory")
    model_transparent, geomModel_transparent = rmodel.copy(), rgeomModel.copy()
    for i in range(len(rgeomModel.geometryObjects.tolist())):
        color = geomModel_transparent.geometryObjects[i].meshColor
        color[-1] = .1
        geomModel_transparent.geometryObjects[i].meshColor = color

    visualizers=[]
    for i in range(NbSamples):
        visualizers += [MeshcatVisualizer(model_transparent, geomModel_transparent, geomModel_transparent)]
        visualizers[-1].initViewer(viz.viewer)
        visualizers[-1].loadViewerModel(rootNodeName = "pinocchio"+str(i))
        visualizers[-1].display(positions[0])

    for i,q_i in enumerate(tqdm(positions)):
        viz.display(q_i)
        time.sleep(1./fps)

    if save:
        print("generating frames")
        for t,q_t in enumerate(tqdm(positions)):
            viz.display(q_t)
            for i in range(NbSamples):
                visualizers[i].display(positions_pert[i,t])
            img_t = np.array(viz.viewer.get_image())
            imgs += [img_t]

        writer = imageio.get_writer(name, fps=fps)
        for im in imgs:
            writer.append_data(im)
        writer.close()
    return 

def make_capture(viz, position, name):
    print("generating capture")
    viz.display(position)
    capture = viz.viewer.get_image()
    capture.save(name)
    return 

def make_capture_traj(viz,rmodel,rgeomModel,positions,positions_pert,name,fps):
    imgs = []
    NbSamples = positions_pert.shape[0]
    print("visualizing randomized trajectory")
    model_transparent, geomModel_transparent = rmodel.copy(), rgeomModel.copy()
    for i in range(len(rgeomModel.geometryObjects.tolist())):
        color = geomModel_transparent.geometryObjects[i].meshColor
        color[-1] = .1
        geomModel_transparent.geometryObjects[i].meshColor = color

    visualizers=[]
    for i in range(NbSamples):
        visualizers += [MeshcatVisualizer(model_transparent, geomModel_transparent, geomModel_transparent)]
        visualizers[-1].initViewer(viz.viewer)
        visualizers[-1].loadViewerModel(rootNodeName = "pinocchio"+str(i))
        visualizers[-1].display(positions[0])

    for i,q_i in enumerate(tqdm(positions)):
        viz.display(q_i)
        time.sleep(1./fps)

    print("generating frames")
    for t,q_t in enumerate(tqdm(positions)):
        viz.display(q_t)
        for i in range(NbSamples):
            visualizers[i].display(positions_pert[i,t])
        make_capture(viz, q_t, name/("capture_traj"+str(t)+".png"))
    return 

def RK4Update(model,data,q,v,u,dt):
    a1 = pin.aba(model,data,q,v,u)
    q1,v1 = pin.integrate(model, q,v*dt/2), v+a1*dt/2
    a2 = pin.aba(model,data,q1,v1,u)
    q2,v2 = pin.integrate(model, q,v1*dt/2), v+a2*dt/2
    a3 = pin.aba(model,data,q2,v2,u)
    q3,v3 = pin.integrate(model, q,v2*dt), v+a3*dt
    a4 = pin.aba(model,data,q3,v3,u)
    return pin.integrate(model, q,(dt/3)*((1/2)*v+v1+v2+(1/2)*v3)), v+ (dt/3)*((1/2)*a1 + a2 + a3 + (1/2)*a4)

def EulerUpdate(model,data,q,v,u,dt):
    a1 = pin.aba(model,data,q,v,u)
    return pin.integrate(model, q,v*dt), v+a1*dt

def aba_with_friction(model,data,q,v,u,cf,dt):
    a = pin.aba(model,data,q,v,u)
    Minv = np.linalg.inv(pin.crba(model,data,q))
    af = np.where(np.abs(v) < 1e-3, 
                  np.sign(a)*np.clip(np.abs(a)-np.abs(np.dot(Minv,np.sign(a)*cf)),0,None),
                  a-np.dot(Minv,np.sign(v)*cf))
    vnew = v + dt*af
    return vnew, [0]

def aba_with_friction2(model,data,q,v,u,cf,dt): #impulse formulation
    a = pin.aba(model,data,q,v,u)
    vfree = v + a*dt
    Minv = np.linalg.inv(pin.crba(model,data,q))
    P = dt*torch.from_numpy(Minv).unsqueeze(0)
    p = torch.from_numpy(vfree).unsqueeze(0).unsqueeze(-1)
    l_min = -torch.from_numpy(cf).unsqueeze(0).unsqueeze(-1)
    l_max = torch.from_numpy(cf).unsqueeze(0).unsqueeze(-1)
    warm_start = torch.zeros_like(l_min)
    eps, max_iter = 1e-8, 100
    tau_f = BoxQPFn2().apply(P,p,l_min,l_max, warm_start, eps, max_iter).detach().squeeze(0).squeeze(-1).numpy()
    vnew = vfree + np.dot(Minv,tau_f)*dt
    return vnew, tau_f

def aba_with_friction3(model,data,q,v,u,cf,dt): # acc formulation wo power constraint
    a = pin.aba(model,data,q,v,u)
    Minv = np.linalg.inv(pin.crba(model,data,q))
    P = dt*torch.from_numpy(Minv).unsqueeze(0)
    p = torch.from_numpy(a).unsqueeze(0).unsqueeze(-1)
    l_min = -torch.from_numpy(cf).unsqueeze(0).unsqueeze(-1)
    l_max = torch.from_numpy(cf).unsqueeze(0).unsqueeze(-1)
    warm_start = torch.zeros_like(l_min)
    eps, max_iter = 1e-8, 100
    tau_f = BoxQPFn2().apply(P,p,l_min,l_max, warm_start, eps, max_iter).detach().squeeze(0).squeeze(-1).numpy()
    vnew = v + dt*(a + np.dot(Minv,tau_f))
    return vnew, tau_f

def aba_with_friction4(model,data,q,v,u,cf,dt): # acc formulation with power constraint
    a = pin.aba(model,data,q,v,u)
    Minv = np.linalg.inv(pin.crba(model,data,q))
    P = torch.from_numpy(Minv).unsqueeze(0)
    p = torch.from_numpy(a).unsqueeze(0).unsqueeze(-1)
    vt = torch.from_numpy(v).unsqueeze(0).unsqueeze(-1)
    l_min = -torch.from_numpy(cf).unsqueeze(0).unsqueeze(-1)
    l_max = torch.from_numpy(cf).unsqueeze(0).unsqueeze(-1)
    #warm_start = torch.zeros_like(l_min)
    warm_start = torch.where(vt<0,l_max,l_min)
    eps, max_iter = 1e-8, 100
    tau_f = SignedBoxQPFn2().apply(P,p,l_min,l_max, vt, warm_start, eps, max_iter).detach().squeeze(0).squeeze(-1).numpy()
    #print(P,q,l_min,l_max, vt)
    vnew = v + dt*(a + np.dot(Minv,tau_f))
    #if np.any(tau_f==0):
    #    print(P,p,l_min,l_max, vt,q)
    return vnew, tau_f

def aba_with_friction5(model,data,q,v,u,cf,dt):
    a = pin.aba(model,data,q,v,u)
    Minv = np.linalg.inv(pin.crba(model,data,q))
    af = np.where(np.abs(v) < 1e-3, 
                  np.sign(a)*np.clip(np.abs(a)-np.abs(np.dot(Minv,np.sign(a)*cf)),0,None),
                  a-np.dot(Minv,np.sign(v)*cf))
    return af, [0]


def RK4Update_with_friction(model,data,q,v,u,dt,cf):
    a1 = aba_with_friction(model,data,q,v,u,cf)
    q1,v1 = pin.integrate(model, q,v*dt/2), v+a1*dt/2
    a2 = aba_with_friction(model,data,q1,v1,u,cf)
    q2,v2 = pin.integrate(model, q,v1*dt/2), v+a2*dt/2
    a3 = aba_with_friction(model,data,q2,v2,u,cf)
    q3,v3 = pin.integrate(model, q,v2*dt), v+a3*dt
    a4 = aba_with_friction(model,data,q3,v3,u,cf)
    return pin.integrate(model, q,(dt/3)*((1/2)*v+v1+v2+(1/2)*v3)), v+ (dt/3)*((1/2)*a1 + a2 + a3 + (1/2)*a4)


def create_pendulum(N):
    #base is fixed
    model = pin.Model()
    geom_model = pin.GeometryModel()

    parent_id = 0
    base_radius = 0.2
    shape_base = fcl.Sphere(base_radius)
    geom_base = pin.GeometryObject("base", 0, shape_base, pin.SE3.Identity())
    geom_base.meshColor = np.array([1.,0.1,0.1,1.])
    geom_model.addGeometryObject(geom_base)
    joint_placement = pin.SE3.Identity()
    body_mass = 1.
    body_radius = 0.1
    for k in range(N):
        joint_name = "joint_" + str(k+1)
        joint_id = model.addJoint(parent_id,pin.JointModelRX(),joint_placement,joint_name)

        body_inertia = pin.Inertia.FromSphere(body_mass,body_radius)
        body_placement = joint_placement.copy()
        body_placement.translation[2] = 1.
        model.appendBodyToJoint(joint_id,body_inertia,body_placement)

        geom1_name = "ball_" + str(k+1)
        shape1 = fcl.Sphere(body_radius)
        geom1_obj = pin.GeometryObject(geom1_name, joint_id, shape1, body_placement)
        geom1_obj.meshColor = np.ones((4))
        geom_model.addGeometryObject(geom1_obj)

        geom2_name = "bar_" + str(k+1)
        shape2 = fcl.Cylinder(body_radius/4.,body_placement.translation[2])
        shape2_placement = body_placement.copy()
        shape2_placement.translation[2] /= 2.

        geom2_obj = pin.GeometryObject(geom2_name, joint_id, shape2, shape2_placement)
        geom2_obj.meshColor = np.array([0.,0.,0.,1.])
        geom_model.addGeometryObject(geom2_obj)

        parent_id = joint_id
        joint_placement = body_placement.copy()
    end_frame=pin.Frame("end_effector_frame",model.getJointId("joint_"+str(N)), 0, body_placement, pin.FrameType(3))
    model.addFrame(end_frame)
    geom_model.collision_pairs = []
    model.qinit = np.zeros(model.nq)
    model.qinit[0] = 1.*np.pi
    model.qref = pin.neutral(model)
    data = model.createData()
    geom_data = geom_model.createData()
    ddl = np.array([0])
    return model, geom_model, data, geom_data, ddl

def create_double_pendulum_erd():
    robot = erd.load("double_pendulum")
    model = robot.model
    end_placement = pin.SE3.Identity()
    end_placement.translation = np.array([0.,0.,0.2])
    end_frame = pin.Frame("end_effector_frame", model.getJointId("joint2"), 0, end_placement, pin.FrameType(3))
    model.addFrame(end_frame)
    model.qinit = np.zeros(model.nq)
    model.qinit[0] = 1.*np.pi
    model.qref = pin.neutral(model)
    data = model.createData()
    geom_model = robot.collision_model
    geom_model.collision_pairs = []
    geom_data = robot.collision_data
    ddl = np.array([0,1])
    return model, geom_model, data, geom_data, ddl

def create_acrobot():
    robot = erd.load("double_pendulum")
    model = robot.model
    end_placement = pin.SE3.Identity()
    end_placement.translation = np.array([0.,0.,0.2])
    end_frame = pin.Frame("end_effector_frame", model.getJointId("joint2"), 0, end_placement, pin.FrameType(3))
    model.addFrame(end_frame)
    model.qinit = np.zeros(model.nq)
    model.qinit[0] = 1.*np.pi
    model.qref = pin.neutral(model)
    data = model.createData()
    geom_model = robot.collision_model
    geom_model.collision_pairs = []
    geom_data = robot.collision_data
    ddl = np.array([1])
    return model, geom_model, data, geom_data, ddl

def create_cartpole(N):
    model = pin.Model()
    geom_model = pin.GeometryModel()
    
    parent_id = 0
    
    cart_radius = 0.1
    cart_length = 5 * cart_radius
    cart_mass = 2.
    joint_name = "joint_cart"

    geometry_placement = pin.SE3.Identity()
    geometry_placement.rotation = pin.Quaternion(np.array([0.,0.,1.]),np.array([0.,1.,0.])).toRotationMatrix()

    joint_id = model.addJoint(parent_id, pin.JointModelPY(), pin.SE3.Identity(), joint_name)

    body_inertia = pin.Inertia.FromCylinder(cart_mass,cart_radius,cart_length)
    body_placement = geometry_placement
    model.appendBodyToJoint(joint_id,body_inertia,body_placement) # We need to rotate the inertia as it is expressed in the LOCAL frame of the geometry

    shape_cart = fcl.Cylinder(cart_radius, cart_length)

    geom_cart = pin.GeometryObject("shape_cart", joint_id, shape_cart, geometry_placement)
    geom_cart.meshColor = np.array([1.,0.1,0.1,1.])
    geom_model.addGeometryObject(geom_cart)

    parent_id = joint_id
    joint_placement = pin.SE3.Identity()
    body_mass = 1.
    body_radius = 0.1
    for k in range(N):
        joint_name = "joint_" + str(k+1)
        joint_id = model.addJoint(parent_id,pin.JointModelRX(),joint_placement,joint_name)

        body_inertia = pin.Inertia.FromSphere(body_mass,body_radius)
        body_placement = joint_placement.copy()
        body_placement.translation[2] = 1.
        model.appendBodyToJoint(joint_id,body_inertia,body_placement)

        geom1_name = "ball_" + str(k+1)
        shape1 = fcl.Sphere(body_radius)
        geom1_obj = pin.GeometryObject(geom1_name, joint_id, shape1, body_placement)
        geom1_obj.meshColor = np.ones((4))
        geom_model.addGeometryObject(geom1_obj)

        geom2_name = "bar_" + str(k+1)
        shape2 = fcl.Cylinder(body_radius/4.,body_placement.translation[2])
        shape2_placement = body_placement.copy()
        shape2_placement.translation[2] /= 2.

        geom2_obj = pin.GeometryObject(geom2_name, joint_id, shape2, shape2_placement)
        geom2_obj.meshColor = np.array([0.,0.,0.,1.])
        geom_model.addGeometryObject(geom2_obj)

        parent_id = joint_id
        joint_placement = body_placement.copy()
    end_frame=pin.Frame("end_effector_frame", model.getJointId("joint_"+str(N)), 0, body_placement, pin.FrameType(3))
    # end_frame=pin.Frame("end_effector_frame",model.getFrameId(str(parent_id)), parent_id, body_placement, pin.FrameType(3))
    model.addFrame(end_frame)
    geom_model.collision_pairs = []
    model.qinit = np.zeros(model.nq)
    model.qinit[1] = 1.*np.pi
    model.qref = pin.neutral(model)
    data = model.createData()
    geom_data = geom_model.createData()
    ddl = np.array([0])
    return model, geom_model, data, geom_data, ddl

def createSoloModel():
    # robot model
    robot = example_robot_data.load('solo12')
    rmodel = robot.model.copy()
    rdata = rmodel.createData()
    rmodel.qref = rmodel.referenceConfigurations["standing"]
    rmodel.qinit = rmodel.referenceConfigurations["standing"]
    # Geometry model
    rgeomModel = robot.collision_model
    # add feet
    a = 0.01910275#1730829468
    rFoot = np.array([0.2, 0.08, 0.05])
    R = np.eye(3)
    tLeft = np.array([0, 0.008,  -0.16])
    MLeft = pin.SE3(R, tLeft)
    tRight = np.array([0, -0.008,  -0.16])
    MRight = pin.SE3(R, tRight)
    r = np.array([a, a, a])  
    
    
    rgeomModel.computeColPairDist = []

    n = np.array([0., 0., 1])
    p = np.array([0., 0., 0.0])
    h = np.array([100., 100., 0.01])
    plane_shape = Plane(0, 'plane', n, p, h)
    T = pin.SE3(plane_shape.R, plane_shape.t)
    plane = pin.GeometryObject("plane", 0, 0, plane_shape, T)
    plane.meshColor = np.array([0.5, 0.5, 0.5, 1.]) 
    planeId = rgeomModel.addGeometryObject(plane)
    
    frames_names = ["HR_FOOT","HL_FOOT","FR_FOOT","FL_FOOT"]
    
    
    rgeomModel.collision_pairs = []
    for name in frames_names:
        frame_id = rmodel.getFrameId(name)
        frame = rmodel.frames[frame_id]
        joint_id = frame.parent
        frame_placement = frame.placement
        
        shape_name = name + "_shape"
        shape = Ellipsoid(joint_id, shape_name , r, frame_placement)
        geometry = pin.GeometryObject(shape_name, joint_id, shape, frame_placement)
        geometry.meshColor = np.array([1.0, 0.2, 0.2, 0.])
        
        geom_id = rgeomModel.addGeometryObject(geometry)
        
        foot_plane = CollisionPairPlaneEllipsoid(planeId, geom_id)
        rgeomModel.collision_pairs += [foot_plane]
        rgeomModel.computeColPairDist.append(False)
    rgeom_data = rgeomModel.createData()
    ddl = np.array([i for i in range(6,rmodel.nv)])
    return rmodel, rgeomModel, rdata, rgeom_data, ddl

def createCubeModel():
    rmodel = pin.Model()
    freeflyer = pin.JointModelFreeFlyer()
    jointCube = rmodel.addJoint(0, freeflyer, pin.SE3.Identity(), "joint1")
    M = pin.SE3(np.eye(3), np.matrix([0., 0., 0.]).T)
    rmodel.appendBodyToJoint(jointCube, pin.Inertia.FromBox(1,0.2,0.2,0.2), M)
    rmodel.qref = pin.neutral(rmodel)
    rmodel.qinit = rmodel.qref.copy()
    rmodel.qinit[2] += 0.1
    data = rmodel.createData()
    rgeomModel = pin.GeometryModel()
    a = 0.2
    r = np.array([a/4, a/4, a/4])
    cube_shape = fcl.Box(a, a, a)
    rgeomModel.computeColPairDist = []

    n = np.array([0., 0., 1])
    p = np.array([0., 0., 0.0])
    h = np.array([10., 10., 0.])
    plane_shape = Plane(0, 'plane', n, p, h)
    T = pin.SE3(plane_shape.R, plane_shape.t)
    plane = pin.GeometryObject("plane", 0, 0, plane_shape, T)
    plane.meshColor = np.array([0.5, 0.5, 0.5, 1.]) 
    planeId = rgeomModel.addGeometryObject(plane)

    # add balls to cube
    rgeomModel.collision_pairs = []

    R = pin.utils.eye(3)
    t = np.matrix([a/4, -a/4, -a/4]).T
    ball_shape1 = Ellipsoid(jointCube, 'ball1', r,pin.SE3(R, t)) 
    geom_ball1 = pin.GeometryObject("ball1", jointCube, jointCube, ball_shape1, pin.SE3(R, t)) 
    geom_ball1.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
    ball1Id = rgeomModel.addGeometryObject(geom_ball1)
    col_pair1 = CollisionPairPlaneEllipsoid(planeId, ball1Id)
    col_pair1.computeDistanceResidualBool = True
    rgeomModel.collision_pairs += [col_pair1]

    R = pin.utils.eye(3)
    t = np.matrix([a/4, a/4, -a/4]).T
    ball_shape2 = Ellipsoid(jointCube, 'ball2', r,pin.SE3(R, t)) 
    geom_ball2 = pin.GeometryObject("ball2", jointCube, jointCube, ball_shape2, pin.SE3(R, t)) 
    geom_ball2.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
    ball2Id = rgeomModel.addGeometryObject(geom_ball2)
    col_pair2 = CollisionPairPlaneEllipsoid(planeId, ball2Id)
    col_pair2.computeDistanceResidualBool = True
    rgeomModel.collision_pairs += [col_pair2]

    R = pin.utils.eye(3)
    t = np.matrix([a/4, a/4, a/4]).T
    ball_shape3 = Ellipsoid(jointCube, 'ball3', r,pin.SE3(R, t)) 
    geom_ball3 = pin.GeometryObject("ball3", jointCube, jointCube, ball_shape3, pin.SE3(R, t)) 
    geom_ball3.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
    ball3Id = rgeomModel.addGeometryObject(geom_ball3)
    col_pair3 = CollisionPairPlaneEllipsoid(planeId, ball3Id)
    col_pair3.computeDistanceResidualBool = True
    rgeomModel.collision_pairs += [col_pair3]

    R = pin.utils.eye(3)
    t = np.matrix([a/4, -a/4, a/4]).T
    ball_shape4 = Ellipsoid(jointCube, 'ball4', r,pin.SE3(R, t)) 
    geom_ball4 = pin.GeometryObject("ball4", jointCube, jointCube, ball_shape4, pin.SE3(R, t)) 
    geom_ball4.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
    ball4Id = rgeomModel.addGeometryObject(geom_ball4)
    col_pair4 = CollisionPairPlaneEllipsoid(planeId, ball4Id)
    col_pair4.computeDistanceResidualBool = True
    rgeomModel.collision_pairs += [col_pair4]

    R = pin.utils.eye(3)
    t = np.matrix([-a/4, -a/4, -a/4]).T
    ball_shape5 = Ellipsoid(jointCube, 'ball5', r,pin.SE3(R, t)) 
    geom_ball5 = pin.GeometryObject("ball5", jointCube, jointCube, ball_shape5, pin.SE3(R, t)) 
    geom_ball5.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
    ball5Id = rgeomModel.addGeometryObject(geom_ball5)
    col_pair5 = CollisionPairPlaneEllipsoid(planeId, ball5Id)
    col_pair5.computeDistanceResidualBool = True
    rgeomModel.collision_pairs += [col_pair5]

    R = pin.utils.eye(3)
    t = np.matrix([-a/4, a/4, -a/4]).T
    ball_shape6 = Ellipsoid(jointCube, 'ball6', r,pin.SE3(R, t)) 
    geom_ball6 = pin.GeometryObject("ball6", jointCube, jointCube, ball_shape6, pin.SE3(R, t)) 
    geom_ball6.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
    ball6Id = rgeomModel.addGeometryObject(geom_ball6)
    col_pair6 = CollisionPairPlaneEllipsoid(planeId, ball6Id)
    col_pair6.computeDistanceResidualBool = True
    rgeomModel.collision_pairs += [col_pair6]

    R = pin.utils.eye(3)
    t = np.matrix([-a/4, a/4, a/4]).T
    ball_shape7 = Ellipsoid(jointCube, 'ball7', r,pin.SE3(R, t)) 
    geom_ball7 = pin.GeometryObject("ball7", jointCube, jointCube, ball_shape7, pin.SE3(R, t)) 
    geom_ball7.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
    ball7Id = rgeomModel.addGeometryObject(geom_ball7)
    col_pair7 = CollisionPairPlaneEllipsoid(planeId, ball7Id)
    col_pair7.computeDistanceResidualBool = True
    rgeomModel.collision_pairs += [col_pair7]

    R = pin.utils.eye(3)
    t = np.matrix([-a/4, -a/4, a/4]).T
    ball_shape8 = Ellipsoid(jointCube, 'ball8', r,pin.SE3(R, t)) 
    geom_ball8 = pin.GeometryObject("ball8", jointCube, jointCube, ball_shape8, pin.SE3(R, t)) 
    geom_ball8.meshColor = np.array([1.0, 0.2, 0.2, 1.]) 
    ball8Id = rgeomModel.addGeometryObject(geom_ball8)
    col_pair8 = CollisionPairPlaneEllipsoid(planeId, ball8Id)
    col_pair8.computeDistanceResidualBool = True
    rgeomModel.collision_pairs += [col_pair8]

    R = pin.utils.eye(3)
    t = np.matrix([0., 0., 0.]).T
    box_shape = fcl.Box(a,a,a) 
    geom_box = pin.GeometryObject("box", jointCube, jointCube, box_shape, pin.SE3(R, t)) 
    geom_box.meshColor = np.array([1.0, 0.2, 0.2, 1.])
    boxId = rgeomModel.addGeometryObject(geom_box)

    rgeom_data = rgeomModel.createData()
    ddl = np.array([2])
    return rmodel, rgeomModel, data, rgeom_data, ddl


class SmoothContactActionModel(croc.ActionModelAbstract):
    def __init__(self, state, ddl, simulator, jointCube, ptarget1, utarget , cw1, cw2, noiseIntensity, nbSamples, adaptive=False):
        self.sim = simulator
        self.ptarget1 = ptarget1
        self.utarget = utarget
        self.jointCube = jointCube
        self.costWeight1 = cw1
        self.costWeight2 = cw2
        self.nv = self.sim.rmodel.nv 
        self.noiseIntensity = noiseIntensity
        self.nbSamples = nbSamples
        self.ddl = ddl
        nu = len(ddl)
        nr = self.nv #+ len(ddl)
        self.unone = np.zeros(nu)
        self.uNoise = None
        self.adaptive = adaptive
        croc.ActionModelAbstract.__init__(self, state, nu, nr)

    def createData(self):
        data = croc.ActionModelAbstract.createData(self)
        data.node = SimulatorNode(self.sim)
        return data
    
    def calc(self, data, x, u=None, uNoise = None):
        if u is None:
            u = self.unone
        if uNoise is None:
            uNoise = self.uNoise if self.uNoise is not None else torch.zeros((self.nbSamples, u.shape[0]))
        t_x = torch.from_numpy(x)
        t_xnext = torch.zeros_like(t_x)
        self.xis = np.zeros((self.nbSamples,2*self.state.nv))
        for i,ui in enumerate(uNoise):
            t_tau = torch.zeros(self.nv)
            t_tau[self.ddl] = torch.tensor(u) + ui
            t_xnexti = data.node.makeStep(t_x, t_tau, calcPinDiff=False)
            t_xnext += t_xnexti
            self.xis[i,:] = t_xnexti.detach().numpy()
        t_xnext /= self.nbSamples
        x_tar = torch.zeros_like(t_xnext)
        #x_tar[2] = self.ptarget1[2]
        x_tar = torch.tensor(self.ptarget1)
        t_res1 = torch.from_numpy(np.sqrt(self.costWeight1))*(t_xnext[:self.nv] - x_tar)
        data.xnext[:] = t_xnext.detach().numpy()
        t_res_u = torch.from_numpy(np.sqrt(self.costWeight2) * (u- self.utarget))
        #print("t_resu",t_res_u.size())
        t_res = torch.cat([t_res1, t_res_u])
        data.r = t_res.detach().numpy()
        #print("xnext",t_xnext,"xtar", x_tar)
        #print(t_res1)
        data.cost =  0.5 * sum(data.r**2)
        return data.xnext, data.cost


    def calcDiff(self, data, x, u=None):
        t_x = torch.from_numpy(x).requires_grad_(True)
        t_u = torch.from_numpy(u).requires_grad_(True)
        if u is None:
            u = self.unone
        uNoise = self.noiseIntensity*torch.randn((self.nbSamples, t_u.size()[0]))
        self.uNoise = uNoise
        xnext, cost = self.calc(data, x, u)
        nbSamples_concu = 1 if self.adaptive else self.nbSamples
        def calcTorch(t_x, t_u, uNoise):
            t_xnext = torch.zeros_like(t_x)
            for i in range(nbSamples_concu): 
                t_tau = torch.zeros(self.nv)
                t_tau[self.ddl] = t_u + uNoise[i]
                t_xnexti = data.node.makeStep(t_x, t_tau, calcPinDiff=True)
                t_xnext += t_xnexti
            t_xnext /= nbSamples_concu
            #x_tar = torch.zeros_like(t_xnext)
            x_tar = torch.tensor(self.ptarget1)
            t_res1 = torch.from_numpy(np.sqrt(self.costWeight1))*(t_xnext[:self.nv] - x_tar)#.unsqueeze(0)
            t_res_reg =  torch.tensor([0.]) 
            t_res_u = torch.from_numpy(np.sqrt(self.costWeight2)) * (t_u - torch.from_numpy(self.utarget))
            t_res = torch.cat([t_res1, t_res_u, t_res_reg])
            t_cost = 0.5 * torch.sum(t_res * t_res)
            return t_xnext, t_cost
        if self.adaptive:
            data.Fx[:] = 0
            data.Fu[:] = 0
            data.Lx[:] = 0
            data.Lu[:] = 0
            data.Luu[:] = np.eye(u.shape[0])*(self.costWeight2)
            data.Lxu[:] = 0
            #self.Lus = np.zeros((self.nbSamples, len(self.ddl)))
            self.Fus = np.zeros((self.nbSamples,2*self.state.nv, len(self.ddl)))
            for i in range(self.nbSamples):    
                Ji = torch.autograd.functional.jacobian(calcTorch, (t_x, t_u, uNoise[None,i]))
                data.Fx[:] += Ji[0][0].squeeze().numpy()/self.nbSamples
                data.Fu[:] += Ji[0][1][:].squeeze().numpy()/self.nbSamples
                data.Lx[:] += Ji[1][0].numpy()/self.nbSamples
                data.Lu[:] += Ji[1][1].numpy()/self.nbSamples
                #self.Lus[i] = Ji[1][1].numpy()
                self.Fus[i] = Ji[0][1][:].numpy()
        else:
            J = torch.autograd.functional.jacobian(calcTorch, (t_x, t_u, uNoise))
            data.Fx[:] = J[0][0].squeeze().numpy()
            data.Fu[:] = J[0][1][:].squeeze().numpy()
            data.Lx[:] = J[1][0].numpy()
            data.Lu[:] = J[1][1].numpy()
            data.Luu[:] = np.eye(u.shape[0])*(self.costWeight2)
            data.Lxu[:] = 0
        if np.linalg.norm(data.Fu[:])<1e-9:
            data.Fu[:] = 0
        return xnext, cost

class SmoothFrictionActionModel(croc.DifferentialActionModelAbstract):
    def __init__(self, state, ddl, costModel, cf, dt, noiseIntensity, nbSamples, pool):
        nu = len(ddl)
        croc.DifferentialActionModelAbstract.__init__(self, state, nu, costModel.nr)
        self.costs = costModel
        #self.enable_force = True
        #self.armature = np.zeros(0)
        self.nbSamples = nbSamples
        self.noiseIntensity = noiseIntensity
        self.dt = dt
        self.cf = cf
        self.pool = pool
        if pool is not None:
            self.models_pin = [self.state.pinocchio.copy() for i in range(nbSamples)]
        self.ddl = ddl
        self.VarFu = 0
        self.uNoise = None


    def calc(self, data, x, u=None, uNoise = None):
        if uNoise is None:
            uNoise = self.uNoise if self.uNoise is not None else self.noiseIntensity*np.random.normal(size=(self.nbSamples, len(self.ddl)))
        if u is None:
            u = self.unone
        q, v = x[:self.state.nq], x[-self.state.nv:]
        # Computing the dynamics using ABA or manually for armature case
        data.xout[:] = 0
        data.ais = np.zeros((self.nbSamples,self.state.nv))
        self.xis = np.zeros((self.nbSamples,self.state.nq +self.state.nv))
        tau = np.zeros(self.state.pinocchio.nv)
        tau[self.ddl] = u 
        tau_pert = np.zeros((self.nbSamples,self.state.pinocchio.nv))
        tau_pert[:,self.ddl] = tau[self.ddl] +uNoise
        if self.pool is not None:
            args = [(self.models_pin[i], self.data_pin[i], q, v, tau_pert[i], self.cf,  self.dt) for i in range(self.nbSamples)]
            results_samples = self.pool.starmap(aba_with_friction5, args)
            data.ais = [results_samples[i][0] for i in range(len(results_samples))]
            data.xout += sum(data.ais)/self.nbSamples
        else:
            for i in range(self.nbSamples):
                a_i, _ = aba_with_friction5(self.state.pinocchio, data.pinocchio, q, v, tau_pert[i], self.cf, self.dt)
                data.xout += a_i/self.nbSamples
                data.ais[i] = a_i
                self.xis[i][self.state.nq:] = v + a_i*self.dt
                self.xis[i][:self.state.nq] = pin.integrate(self.state.pinocchio, q, self.xis[i][self.state.nq:]*self.dt)
        # Computing the cost value and residuals
        pin.forwardKinematics(self.state.pinocchio, data.pinocchio, q, v)
        pin.updateFramePlacements(self.state.pinocchio, data.pinocchio)
        if self.pool is not None:
            #self.data_pin = self.pool.starmap(lambda data: data.copy(), [data.pinocchio]*self.nbSamples)
            self.data_pin = [data.pinocchio.copy() for i in range(self.nbSamples)]
        self.costs.calc(data.costs, x, u)
        data.cost = data.costs.cost
        #print(data.cost)
        


    def calcDiff(self, data, x, u=None):
        q, v = x[:self.state.nq], x[-self.state.nv:]
        if u is None:
            u = self.unone
        uNoise = self.noiseIntensity*np.random.normal(size=(self.nbSamples, len(self.ddl)))
        self.uNoise = uNoise
        self.calc(data, x, u, uNoise)
        # Computing the dynamics derivatives
        data.Fx[:] = 0
        data.Fu[:] = 0
        self.Fus = np.zeros((self.nbSamples,self.state.nq+self.state.nv, len(self.ddl)))
        tau = np.zeros(self.state.pinocchio.nv)
        tau[self.ddl] = u 
        tau_pert = np.zeros((self.nbSamples,self.state.pinocchio.nv))
        tau_pert[:,self.ddl] = tau[self.ddl] +uNoise
        for j in range(self.nbSamples):
            pin.computeABADerivatives(self.state.pinocchio, data.pinocchio, q, v, tau_pert[j])
            Fxi = np.hstack([data.pinocchio.ddq_dq, data.pinocchio.ddq_dv])
            Fui = data.pinocchio.Minv[:,self.ddl]
            for i in range(self.state.nv):
                if np.abs(v)[i]<1e-3:
                    if data.ais[j][i] != 0:
                        continue
                    else:
                        #Fxi[i][:] = 0
                        Fui[i][:] = 0
                else:
                    continue
            Fqu = (self.dt**2)*np.dot(pin.dIntegrate(self.state.pinocchio,q,v +data.ais[j]*self.dt,pin.ArgumentPosition.ARG1),Fui)
            Fvu = self.dt*Fui
            #print(j, "Fvu" ,Fvu)
            #print("v",v, "ais",data.ais)
            self.Fus[j] = np.concatenate((Fqu,Fvu),axis=0)
            if Fxi.shape[0] == 1:
                Fxi = np.squeeze(Fxi, axis = 0)
            if Fui.shape[1] == 1:
                Fui = np.squeeze(Fui, axis = 1)
            data.Fx[:] += Fxi/self.nbSamples 
            data.Fu[:] += Fui[:]/self.nbSamples
        # Computing the cost derivatives
        self.costs.calcDiff(data.costs, x, u)

    def createData(self):
        data = croc.DifferentialActionModelAbstract.createData(self)
        data.pinocchio = self.state.pinocchio.createData()#pin.Data(self.state.pinocchio)
        data.multibody = croc.DataCollectorMultibody(data.pinocchio)
        data.costs = self.costs.createData(data.multibody)
        data.costs.shareMemory(data) # this allows us to share the memory of cost-terms of action model#
        if self.pool is not None:
            self.data_pin = [data.pinocchio.copy() for i in range(self.nbSamples)]

        return data


def smooth_friction_action_model(state, ddl, Mref, u_target, wet_cur, wet_final, wu_reg, wx_cur, wx_final, wx_reg,cf,noiseIntensity,nbSamples, pool,DT):
    efTranslationCost = croc.CostModelFrameTranslation(state, Mref,len(ddl))
    xRegCost = croc.CostModelState(state, croc.ActivationModelWeightedQuad(wx_reg), np.zeros_like(wx_reg),len(ddl))
    #u_target = np.zeros(state.pinocchio.nv) #target control to regularize cost function
    #wu_reg_tmp = np.zeros_like(u_target)
    #print("u_tar",u_target, wu_reg)
    #wu_reg_tmp[ddl] = wu_reg
    uRegCost = croc.CostModelControl(state, croc.ActivationModelWeightedQuad(wu_reg), u_target)
    #uRegCost = croc.CostModelControl(state, croc.ActivationModelWeightedQuad(wu_reg_tmp), u_target)
    xCost_cur = croc.CostModelState(state, croc.ActivationModelWeightedQuad(wx_cur), np.zeros_like(wx_cur),len(ddl))
    xCost_final = croc.CostModelState(state, croc.ActivationModelWeightedQuad(wx_final), np.zeros_like(wx_final),len(ddl))
    # Create cost model per each action model
    runningCostModel = croc.CostModelSum(state, len(ddl))
    terminalCostModel = croc.CostModelSum(state, len(ddl))
    
    # Then let's add the running and terminal cost functions
    runningCostModel.addCost("endEffectorPose", efTranslationCost, wet_cur[0]/DT)
    runningCostModel.addCost("modelState", xCost_cur, 1e-0/DT)
    runningCostModel.addCost("stateReg", xRegCost, 1e0/DT)
    runningCostModel.addCost("ctrlReg", uRegCost, 1e0/DT)
    terminalCostModel.addCost("endEffectorPose", efTranslationCost, wet_final[0])
    terminalCostModel.addCost("modelState", xCost_final, 1e-0)
    terminalCostModel.addCost("stateReg", xRegCost, 1e0)
    terminalCostModel.addCost("ctrlReg", uRegCost, 1e0)
    pendulumDAM = SmoothFrictionActionModel(state, ddl, runningCostModel, cf, DT, noiseIntensity, nbSamples,pool)
    pendulumDAM.createData()
    terminalPendulumDAM = SmoothFrictionActionModel(state, ddl, terminalCostModel,cf, DT, noiseIntensity, nbSamples, pool)
    terminalPendulumDAM.createData()
    runningModel = croc.IntegratedActionModelEuler(
        pendulumDAM, DT)
    runningModel.nbSamples = pendulumDAM.nbSamples
    runningModel.noiseIntensity = pendulumDAM.noiseIntensity
    terminalModel = croc.IntegratedActionModelEuler(
        terminalPendulumDAM, 0.)
    terminalModel.nbSamples = terminalPendulumDAM.nbSamples
    terminalModel.noiseIntensity = terminalPendulumDAM.noiseIntensity
    return  runningModel, terminalModel, pool


def adaptiveSolve(problem, init_xs, init_us, maxIter = 100, qreg=1e-9, alpha = .9, beta = 1., experiment_type="pendulum", adaptive="None", eval_problem=None):
    us = init_us
    xs = init_xs
    T = problem.T
    runningModel = problem.runningModels[0]
    terminalModel = problem.terminalModel
    x0 = problem.x0
    init_Nsamples = runningModel.nbSamples
    init_noise = runningModel.noiseIntensity
    cost = []
    cost_eval = []
    grads = []
    grad_vars = []
    epsilons = [init_noise]
    control_values = [init_us]
    finished=False
    problem = croc.ShootingProblem(x0, [runningModel] * T, terminalModel)
    ddp = croc.SolverFDDP(problem)
    threshold = 5e-1
    for i in trange(maxIter):
        problem = croc.ShootingProblem(x0, [runningModel] * T, terminalModel)
        ddp = croc.SolverFDDP(problem)
        log = croc.CallbackLogger()

        ddp.setCallbacks([log,
                          croc.CallbackVerbose()])
        # Solving it with the DDP algorithm
        finished = ddp.solve(xs, us, 1, True, qreg)
        """print("alpha",ddp.alphas.tolist())
        print("k",ddp.k.tolist())
        print("K", ddp.K.tolist())
        print("Qu", ddp.Qu.tolist())"""
        #print("Quu", ddp.Quu.tolist())
        #print("k", ddp.k.tolist())
        xs = ddp.xs
        us = ddp.us.tolist()
        if eval_problem is not None:
            cost_eval += [eval_problem.calc(xs,us)]
        cost += log.costs
        control_values += [us]
        Qus = ddp.Qu.tolist()
        Qus_inf = np.max([np.max(np.abs(Qu)) for Qu in Qus])
        grads += [Qus_inf]
        if adaptive=="RM":
            runningModel.noiseIntensity /= (i+1) 
            terminalModel.noiseIntensity /= (i+1) 
        elif adaptive=="exp":
            runningModel.noiseIntensity *= alpha 
            runningModel.nbSamples = max(1,int(init_Nsamples*(.99**i)))
            terminalModel.noiseIntensity *= alpha 
            terminalModel.nbSamples = max(1,int(init_Nsamples*(.99**i)))
        elif adaptive=="adaptive":
            Vx = ddp.Vx.tolist()
            Vxx = ddp.Vxx.tolist()
            if experiment_type=="solo" or experiment_type=="cube":
                Fus = [runmodel.Fus for j,runmodel in enumerate(problem.runningModels)]
                #Lus = np.array([runmodel.Lus for j,runmodel in enumerate(problem.runningModels)])
            else:
                Fus = [runmodel.differential.Fus for j,runmodel in enumerate(problem.runningModels)]
                Vxs = [[np.dot(Vxx[j],xi - sum(runmodel.differential.xis)/runningModel.nbSamples) + Vx[j] for xi in runmodel.differential.xis] for j,runmodel in enumerate(problem.runningModels)]
                #print(Fus[:10])
            VxFus = np.zeros((T, runningModel.nbSamples, len(us[0]),1))
            if len(Vx[0].shape)==1:
                Vx = [np.expand_dims(Vxi,axis=1) for Vxi in Vx]
            for k in range(runningModel.nbSamples):
                for l in range(T):
                    VxFus[l,k] = np.dot(Fus[l][k].T, Vxs[l][k])
            #VxFus = VxFus + np.expand_dims(Lus, axis = -1)
            #print(VxFus)
            VarQus = [np.var(VxFu,axis=0) for VxFu in VxFus]
            VarQus_inf = np.max([np.max(VarQu) for VarQu in VarQus])
            if VarQus_inf/(Qus_inf**2) > (0.1)**2:
                runningModel.noiseIntensity *= alpha 
                terminalModel.noiseIntensity *= alpha
            elif  VarQus_inf/(Qus_inf**2) < (1e-4)**2:
                runningModel.noiseIntensity *= beta 
                terminalModel.noiseIntensity *= beta
                #runningModel.nbSamples = max(2,int(init_Nsamples*(.99**i)))
                #terminalModel.nbSamples = max(2,int(init_Nsamples*(.99**i)))
            grad_vars += [VarQus_inf]
        elif adaptive == "cascade":
            Vx = ddp.Vx.tolist()
            Vxx = ddp.Vxx.tolist()
            Quus = ddp.Quu.tolist()
            ks = ddp.k.tolist()
            if experiment_type=="solo" or experiment_type=="cube":
                Fus = [runmodel.Fus for j,runmodel in enumerate(problem.runningModels)]
                Vxs = [[np.dot(Vxx[j],xi - sum(runmodel.xis)/runningModel.nbSamples) + Vx[j] for xi in runmodel.xis] for j,runmodel in enumerate(problem.runningModels)]
            else:
                Fus = [runmodel.differential.Fus for j,runmodel in enumerate(problem.runningModels)]
                Vxs = [[np.dot(Vxx[j],xi - sum(runmodel.differential.xis)/runningModel.nbSamples) + Vx[j] for xi in runmodel.differential.xis] for j,runmodel in enumerate(problem.runningModels)]
            VxFus = np.zeros((T, runningModel.nbSamples, len(us[0]),1))
            if len(Vx[0].shape)==1:
                Vx = [np.expand_dims(Vxi,axis=1) for Vxi in Vx]
            for k in range(runningModel.nbSamples):
                for l in range(T):
                    VxFus[l,k] = np.dot(Fus[l][k].T, Vxs[l][k])[:,None]
            VarQus = [np.var(VxFu,axis=0) for VxFu in VxFus]
            VarQus_inf = np.max([np.max(VarQu) for VarQu in VarQus])
            Qus_inf = np.max([np.dot(Qus[j].T,ks[j]) for j  in range(len(Quus))])
            print(Qus_inf, threshold)
            if Qus_inf < threshold and i > 5:
                print("reducing noise")
                runningModel.noiseIntensity /=  2.
                terminalModel.noiseIntensity /= 2.
                threshold /= 2.
            grad_vars += [VarQus_inf]
        epsilons += [runningModel.noiseIntensity]
        if finished:
            continue
            break
    return finished, ddp, cost, cost_eval, control_values, grads, grad_vars, epsilons